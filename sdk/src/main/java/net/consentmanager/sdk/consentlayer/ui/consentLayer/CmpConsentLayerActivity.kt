package net.consentmanager.sdk.consentlayer.ui.consentLayer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.LinearLayout
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.CmpLayerAppEventListenerInterface
import net.consentmanager.sdk.common.utils.isNetworkAvailable
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import net.consentmanager.sdk.consentlayer.model.CMPConfig.customLayerColor
import net.consentmanager.sdk.consentlayer.model.CmpConsent
import net.consentmanager.sdk.consentlayer.service.CmpConsentService
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpAppInterfaceImpl
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpLayerFragment

/**
 * Cmp consent layer activity
 *
 * @constructor Create empty Cmp consent layer activity
 */
@Keep
internal class CmpConsentLayerActivity : AppCompatActivity() {
    //private final CMPUserConsentService userConsentService;
    private var webView: WebView? = null

    /**
     * On create
     *
     * @param savedInstanceState
     */
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        initWebViewConfig()
        initView()
        moveTaskToBack(false)
        window.setWindowAnimations(0)
        super.onCreate(savedInstanceState)
    }

    /**
     * Override pending transition
     *
     * @param enterAnim
     * @param exitAnim
     */
    override fun overridePendingTransition(enterAnim: Int, exitAnim: Int) {
        super.overridePendingTransition(0, 0)
    }

    private fun initView() {
        // programmatically add a Linear layout webView
        val layoutParams: ViewGroup.LayoutParams
        layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        //new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        val linearLayout = LinearLayout(this)
        linearLayout.addView(webView)
        //Hide Layer
        linearLayout.visibility = LinearLayout.VISIBLE
        addContentView(linearLayout, layoutParams)
    }

    @SuppressLint("SetJavaScriptEnabled", "JavascriptInterface", "AddJavascriptInterface")
    private fun initWebViewConfig() {
        webView = WebView(this)
        val cmpService = CmpConsentService(this.applicationContext)
        webView!!.webViewClient = CmpWebViewClient(this.applicationContext)
        webView!!.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        webView!!.settings.javaScriptEnabled = true
        webView!!.clearCache(true)
        webView!!.clearHistory()
        webView!!.clearFormData()
        webView!!.addJavascriptInterface(
            CmpAppInterfaceImpl(
                null,
                cmpService,
                object : CmpLayerAppEventListenerInterface {
                    override fun onOpen(fragment: CmpLayerFragment?) {
                        runOnUiThread {
                            Log.d("TAG:CMP", "message visible now")
                            javascriptEventOccurred()
                            webView!!.visibility = View.VISIBLE
                        }
                    }

                    override fun onConsentReceived(
                        fragment: CmpLayerFragment?,
                        cmpConsent: CmpConsent,
                    ) {
                        Log.d(TAG, "remove the activity")
                        javascriptEventOccurred()
                        finish()
                    }

                    override fun onError(fragment: CmpLayerFragment?, error: CmpError) {
                        Log.e(TAG, error.toString())
                        finish()
                    }
                }), "Android"
        )
        webView!!.setBackgroundColor(Color.parseColor(customLayerColor))
        webView!!.visibility = View.GONE

        // load URL
        webView!!.loadUrl(url)
        object : CountDownTimer(CMPConfig.timeout.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // no tick actions required
            }

            override fun onFinish() {
                if (timeout) {
                    cmpService.onErrorOccurred(
                        CmpError.TimeoutError,
                        "Timeout Error. The layer has problems to open View. Please try again later"
                    )
                    Log.d(TAG, "The CMP Layer has problems to open View: Please try again later")
                    finish()
                }
            }
        }.start()
    }

    /**
     * On start
     *
     */
    override fun onStart() {
        super.onStart()
        Log.d(TAG, "on Start")
    }

    /**
     * On back pressed
     *
     */
    override fun onBackPressed() {
        Log.d(TAG, "Back Button pressed in WebView")
        if (webView!!.canGoBack()) {
            webView!!.goBack()
        } else {
            val cmpService = CmpConsentService(this.applicationContext)
            cmpService.onCloseAction()
            finish()
        }
    }

    /**
     * On destroy
     *
     */
    override fun onDestroy() {
        super.onDestroy()
        webView!!.clearHistory()
        webView!!.clearCache(true)
        webView!!.removeAllViews()
        webView!!.destroy()
    }

    companion object {
        const val TAG = "CMP:ConsentView"
        var timeout = true
        var url = ""

        /**
         * Used to start the Activity where the consentToolUrl is loaded into WebView.
         *
         * @param context Application Context
         */
        fun openCmpConsentToolView(context: Context, url: String) {
            Companion.url = url
            val cmpService = CmpConsentService(context)
            if (isNetworkAvailable(context)) {
                val cmpConsentToolIntent = Intent(context, CmpConsentLayerActivity::class.java)
                cmpConsentToolIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(cmpConsentToolIntent)
            } else {
                cmpService.onErrorOccurred(
                    CmpError.NetworkError,
                    "The Network is not reachable to show the WebView"
                )
                Log.d(TAG, "The Network is not reachable to show the WebView")
            }
        }

        fun javascriptEventOccurred() {
            timeout = false
        }
    }
}
package net.consentmanager.sdk.common.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.os.Build
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import net.consentmanager.sdk.consentlayer.service.CmpConsentService
import java.util.Calendar
import java.util.Date

// category types applies to vendors and purposes
private const val CUSTOM_CATEGORY_PREFIX = "c"
private const val SYSTEM_CATEGORY_PREFIX = "s"
private const val IAB_CATEGORY_PATTERN = "/^\\d+\$/;"
internal const val EMPTY_DEFAULT_STRING = ""
internal const val EMPTY_DEFAULT_INT = 0

/**
 * Get vendor type
 *
 * @param vendorId
 * @return
 */
internal fun getVendorType(vendorId: String): VendorTypeEnum {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()

    if (pattern.matches(vendorId)) {
        return VendorTypeEnum.IAB_VENDOR
    }
    if (vendorId.startsWith(CUSTOM_CATEGORY_PREFIX)) {
        return VendorTypeEnum.CUSTOM_VENDOR
    }
    if (vendorId.startsWith(SYSTEM_CATEGORY_PREFIX)) {
        return VendorTypeEnum.SYSTEM_VENDOR
    }
    throw IllegalArgumentException("Id '%s' is not a valid vendor")
}

/**
 * Get purpose type
 *
 * @param purposeId
 * @return
 */
internal fun getPurposeType(purposeId: String): PurposeTypeEnum {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()

    if (pattern.matches(purposeId)) {
        return PurposeTypeEnum.IAB_PURPOSE
    }
    if (purposeId.startsWith(CUSTOM_CATEGORY_PREFIX)) {
        return PurposeTypeEnum.CUSTOM_PURPOSE
    }
    if (purposeId.startsWith(SYSTEM_CATEGORY_PREFIX)) {
        return PurposeTypeEnum.SYSTEM_PURPOSE
    }
    throw IllegalArgumentException("Id '%s' is not a valid vendor")
}


/**
 * Method to setup url for the WebView in order to show the last selected values.
 */
internal fun getCmpRequestUrl(
    context: Context,
    forceOpen: Boolean = false,
    rejectAll: Boolean = false,
    cmpStringBase64Encoded: String = "",
): String {
    val cmpService = CmpConsentService(context)
    val cmpString = if (cmpStringBase64Encoded != "") {
        cmpStringBase64Encoded
    } else {
        cmpService.getCmpString()
    }
    val url = ConsentUrlBuilder()
        .setConfig(CMPConfig)
        .setConsent(cmpString)
        .setForceOpen(forceOpen)
        .setRejectAll(rejectAll)
        .build()
    return url
}

/**
 * Is network available
 *
 * @param context Application Context
 * @return
 */
@SuppressLint("ServiceCast")
internal fun isNetworkAvailable(context: Context): Boolean {
    val connManager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connManager.getNetworkCapabilities(connManager.activeNetwork)
        networkCapabilities != null
    } else {
        // below Marshmallow
        val activeNetwork = connManager.activeNetworkInfo
        activeNetwork?.isConnectedOrConnecting == true && activeNetwork.isAvailable
    }
}

/**
 * Vendor type enum
 *
 * @constructor Create empty Vendor type enum
 */
enum class VendorTypeEnum {
    /**
     * Iab Vendor
     *
     * @constructor Create empty Iab Vendor
     */
    IAB_VENDOR,

    /**
     * System Vendor
     *
     * @constructor Create empty System Vendor
     */
    SYSTEM_VENDOR,

    /**
     * Custom Vendor
     *
     * @constructor Create empty Custom Vendor
     */
    CUSTOM_VENDOR;
}

/**
 * Purpose type enum
 *
 * @constructor Create empty Purpose type enum
 */
enum class PurposeTypeEnum {
    /**
     * Iab Purpose
     *
     * @constructor Create empty Iab Purpose
     */
    IAB_PURPOSE,

    /**
     * System Purpose
     *
     * @constructor Create empty System Purpose
     */
    SYSTEM_PURPOSE,

    /**
     * Custom Purpose
     *
     * @constructor Create empty Custom Purpose
     */
    CUSTOM_PURPOSE;
}

/**
 * Check if the date is today
 *
 * @param date
 * @return
 */
fun isDateToday(date: Date?): Boolean {
    if (date == null) {
        return false
    }
    val todayCalendar = Calendar.getInstance()
    val savedCalendar = Calendar.getInstance()
    savedCalendar.time = date

    return todayCalendar[Calendar.YEAR] == savedCalendar[Calendar.YEAR] &&
            todayCalendar[Calendar.MONTH] == savedCalendar[Calendar.MONTH] &&
            todayCalendar[Calendar.DAY_OF_MONTH] == savedCalendar[Calendar.DAY_OF_MONTH]
}
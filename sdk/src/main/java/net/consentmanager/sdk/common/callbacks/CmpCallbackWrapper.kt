package net.consentmanager.sdk.common.callbacks

import android.util.Log
import net.consentmanager.sdk.consentlayer.model.valueObjects.CmpButtonEvent

private const val TAG = "CMP:Callback"
internal const val CALLBACK_TAG = "CMP:Callback"

/**
 * Cmp callback wrapper
 *
 * @constructor Create empty Cmp callback wrapper
 */
internal object CmpCallbackWrapper {
    var onCloseCallback: OnCloseCallback =
        object : OnCloseCallback {
            override fun onWebViewClosed() {
                Log.d(TAG, "closed")
            }
        }
    var onOpenCallback: OnOpenCallback = object : OnOpenCallback {
        override fun onWebViewOpened() {
            Log.d(TAG, "opened")
        }
    }
    var onNotOpenActionCallback: OnCMPNotOpenedCallback =
        object : OnCMPNotOpenedCallback {
            override fun onCMPNotOpened() {
                Log.d(TAG, "not opened")
            }
        }

    var onErrorCallback: OnErrorCallback = object : OnErrorCallback {
    }

    var onCmpButtonClickedCallback: OnCmpButtonClickedCallback =
        object : OnCmpButtonClickedCallback {
            override fun onButtonClicked(event: CmpButtonEvent) {
                Log.d(TAG, event.toString())
            }
        }


    //Setter functions

    /**
     * With close callback
     *
     * @param onCloseCallback
     * @return
     */
    fun withCloseCallback(onCloseCallback: OnCloseCallback?): CmpCallbackWrapper {
        if (onCloseCallback != null) {
            this.onCloseCallback = onCloseCallback
        }
        return this
    }

    /**
     * With open callback
     *
     * @param onOpenCallback
     * @return
     */
    fun withOpenCallback(onOpenCallback: OnOpenCallback?): CmpCallbackWrapper {
        if (onOpenCallback != null) {
            this.onOpenCallback = onOpenCallback
        }
        return this
    }

    /**
     * With not open callback
     *
     * @param onCMPNotOpenedCallback
     * @return
     */
    fun withNotOpenCallback(onCMPNotOpenedCallback: OnCMPNotOpenedCallback?): CmpCallbackWrapper {
        if (onCMPNotOpenedCallback != null) {
            this.onNotOpenActionCallback = onCMPNotOpenedCallback
        }
        return this
    }

    fun withErrorCallback(onErrorCallback: OnErrorCallback?): CmpCallbackWrapper {
        if (onErrorCallback != null) {
            this.onErrorCallback = onErrorCallback
        }
        return this
    }

    fun withButtonClickedCallback(onCmpButtonClickedCallback: OnCmpButtonClickedCallback?): CmpCallbackWrapper {
        if (onCmpButtonClickedCallback != null) {
            this.onCmpButtonClickedCallback = onCmpButtonClickedCallback
        }

        return this
    }
}
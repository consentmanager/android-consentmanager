package net.consentmanager.sdk.consentlayer.service

import android.content.Context
import androidx.preference.PreferenceManager
import java.text.DateFormat
import java.util.Date

/**
 * Cmp preferences
 *
 * @constructor
 *
 * @param context Application Context
 */
internal class CmpPreferences(context: Context) {
    private val sharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
    private val dateFormat = DateFormat.getDateInstance()

    /**
     * Get string
     *
     * @param key
     * @param defaultValue
     * @return
     */
    fun getString(key: String, defaultValue: String): String {
        return sharedPreferences.getString(key, defaultValue) ?: defaultValue
    }

    /**
     * Set string
     *
     * @param key
     * @param value
     */
    fun setString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    /**
     * Get int
     *
     * @param key
     * @param defaultValue
     * @return
     */
    fun getInt(key: String, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    /**
     * Set int
     *
     * @param key
     * @param value
     */
    fun setInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    /**
     * Get boolean
     *
     * @param key
     * @param defaultValue
     * @return
     */
    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    /**
     * Set boolean
     *
     * @param key
     * @param value
     */
    fun setBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    /**
     * Clear
     *
     */
    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    /**
     * Remove key
     *
     * @param key
     */
    fun removeKey(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }

    /**
     * Set date
     *
     * @param key
     * @param date
     */
    fun setDate(key: String, date: Date) {
        val dateString = dateFormat.format(date)
        sharedPreferences.edit().putString(key, dateString).apply()
    }

    /**
     * Get date
     *
     * @param key
     * @param defaultValue
     * @return
     */
    fun getDate(key: String, defaultValue: Date? = null): Date? {
        val dateString = sharedPreferences.getString(key, null)
        return if (dateString != null) {
            try {
                dateFormat.parse(dateString) ?: defaultValue
            } catch (e: Exception) {
                defaultValue
            }
        } else {
            defaultValue
        }
    }
}

package net.consentmanager.sdk.common.callbacks

/**
 * On cmp layer open callback
 *
 */
interface OnCmpLayerOpenCallback {
    /**
     * On open
     *
     */
    fun onOpen()
}


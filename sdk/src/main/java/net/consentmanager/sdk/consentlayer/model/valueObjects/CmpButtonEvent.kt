package net.consentmanager.sdk.consentlayer.model.valueObjects

import net.consentmanager.sdk.consentlayer.model.CmpConsent

sealed class CmpButtonEvent {
    object Unknown : CmpButtonEvent()
    object AcceptAll : CmpButtonEvent()
    object RejectAll : CmpButtonEvent()
    object Save : CmpButtonEvent()
    object Close : CmpButtonEvent()

    override fun toString(): String {
        return when (this) {
            is Unknown -> "Unknown"
            is AcceptAll -> "AcceptAll"
            is RejectAll -> "RejectAll"
            is Save -> "Save"
            is Close -> "Close"
        }
    }
}


fun mapButtonEvent(cmpConsent: CmpConsent?): CmpButtonEvent {
    return when (cmpConsent?.lastButtonEvent) {
        1 -> CmpButtonEvent.AcceptAll
        2 -> CmpButtonEvent.RejectAll
        3 -> CmpButtonEvent.Save
        4 -> CmpButtonEvent.Close
        else -> CmpButtonEvent.Unknown
    }
}


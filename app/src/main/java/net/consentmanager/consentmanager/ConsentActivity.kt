package net.consentmanager.consentmanager

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import net.consentmanager.consentmanager.databinding.ActivityConsentBinding
import net.consentmanager.sdk.CMPConsentTool
import net.consentmanager.sdk.CMPConsentTool.Companion.createInstance
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.CmpImportCallback
import net.consentmanager.sdk.common.callbacks.CmpLayerAppEventListenerInterface
import net.consentmanager.sdk.common.callbacks.OnCMPNotOpenedCallback
import net.consentmanager.sdk.common.callbacks.OnCloseCallback
import net.consentmanager.sdk.common.callbacks.OnCmpButtonClickedCallback
import net.consentmanager.sdk.common.callbacks.OnCmpLayerOpenCallback
import net.consentmanager.sdk.common.callbacks.OnErrorCallback
import net.consentmanager.sdk.common.callbacks.OnOpenCallback
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import net.consentmanager.sdk.consentlayer.model.valueObjects.CmpButtonEvent


class ConsentActivity : AppCompatActivity() {
    private lateinit var binding: ActivityConsentBinding

    companion object {
        const val DEFAULT_CMP_ID: String = "201"
        const val CMP_DOMAIN: String = "delivery.consentmanager.net"
        const val CMP_APP_NAME: String = "Example"
        const val LANG: String = "DE"
        const val TAG: String = "CMP_EXAMPLE_APP"
    }

    private val consentTool: CMPConsentTool by lazy {
        //val layout = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 700)
        //layout.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
        val config = CMPConfig.apply {
            serverDomain = CMP_DOMAIN
            appName = CMP_APP_NAME
            language = LANG
        }
        config.id = getCmpId()
        config.setCustomViewContainerId(R.id.cmpContainer)
        createInstance(
            applicationContext,
            config,
            openListener = object : OnOpenCallback {
                override fun onWebViewOpened() {
                    Log.d(TAG, "opened callback")
                }
            },
            closeListener = object : OnCloseCallback {
                override fun onWebViewClosed() {
                    Log.d(TAG, "closed callback")
                }
            },
            cmpNotOpenedCallback = object : OnCMPNotOpenedCallback {
                override fun onCMPNotOpened() {
                    Log.d(TAG, "cmp not opened")
                }
            },
            errorCallback = object : OnErrorCallback {
                override fun errorOccurred(type: CmpError, message: String) {
                    when (type) {
                        CmpError.NetworkError -> {
                            Log.e(TAG, "Network error: $message")
                            // Handle network error
                        }

                        CmpError.TimeoutError -> {
                            Log.e(TAG, "Timeout error: $message")
                            // Handle timeout error
                        }

                        CmpError.ConsentReadWriteError -> {
                            Log.e(TAG, "Consent read/write error: $message")
                            // Handle consent read/write error
                        }

                        CmpError.ConsentLayerError -> {
                            Log.e(TAG, "Consentlayer error: $message")
                            // Handle consent read/write error
                        }

                        else -> {
                            Log.d(TAG, "default")
                        }
                    }
                }
            },
            cmpButtonClickedCallback = object : OnCmpButtonClickedCallback {
                override fun onButtonClicked(event: CmpButtonEvent) {
                    when (event) {
                        CmpButtonEvent.RejectAll -> {
                            Log.d(TAG, "User clicked Reject all")
                        }

                        CmpButtonEvent.Save -> {
                            Log.d(TAG, "User saved custom settings")
                        }

                        CmpButtonEvent.AcceptAll -> {
                            Log.d(TAG, "User clicked accept all")
                        }

                        CmpButtonEvent.Close -> {
                            Log.d(TAG, "user closed layer without giving consent")
                        }

                        else -> {
                            Log.d(TAG, "no button event logic needed")
                        }
                    }
                }
            }

        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConsentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initClickListener()
    }

    fun consentWillBeShown(): Boolean {
        return true
    }

    private fun initClickListener() {
        binding.buttonCalledLast.setOnClickListener {
            val calledLast = consentTool.getCalledLast(applicationContext)
            Log.d(TAG, "Called Last ${calledLast.toString()}")
        }
        consentTool.setOpenCmpConsentToolViewListener(this, findViewById(R.id.button_show_cmp))

        binding.buttonNeedAcceptance.setOnClickListener {
            val config = CMPConfig
            val consentTool = createInstance(applicationContext, config)
            Log.d(TAG, "hasConsent ${consentTool.hasConsent()}")
//            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }

        binding.buttonExportCmp.setOnClickListener {
            val res = CMPConsentTool.exportCmpString(applicationContext)
            //CMPConsentTool.getStatus(applicationContext)
            Log.d(TAG, "Export CMP: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }

        binding.buttonResetCmp.setOnClickListener {
            Log.d(TAG, "reset all")
            CMPConsentTool.reset(applicationContext)

//            consentTool.rejectAll(applicationContext, object : OnConsentReceivedCallback {
//                override fun onFinish() {
//                    Log.d(TAG, "after Finish; ${consentTool.getAllPurposes(applicationContext)}")
//                }
//            }
//            )
            Log.d(TAG, "logged purposes ${consentTool.getEnabledPurposes(applicationContext)}")
            Log.d(TAG, "logged vendors ${consentTool.getEnabledVendors(applicationContext)}")
//            CMPConsentTool.getStatus(applicationContext)
//            CMPConsentTool.clearAllValues(applicationContext)
        }
        binding.buttonShowCmp.setOnClickListener {
            Log.d(TAG, "check cmp")
            consentTool.check(this, onCmpLayerOpenCallback = object : OnCmpLayerOpenCallback {
                override fun onOpen() {
                    Log.d(TAG, "open feedback given")
                }
            }, isCached = true)
        }
        binding.buttonShowPurposes.setOnClickListener {
            val res = consentTool.getAllPurposes(applicationContext)

            Log.d(TAG, "All Purposes: $res")
            consentTool.getGoogleACString(applicationContext)
            consentTool.getUSPrivacyString(applicationContext)
            Log.d(TAG, "Enabled Purposes: ${consentTool.getAllPurposes(applicationContext)}")
            Log.d(TAG, "All Vendors: ${consentTool.getAllVendorsList(applicationContext)}")
            Log.d(
                TAG,
                "Disagreed Vendors: ${
                    consentTool.getDisabledVendors(applicationContext)
                }"
            )
            Log.d(TAG, "Enabled Vendors: ${consentTool.getEnabledVendors(applicationContext)}")
            CMPConsentTool.importCmpString(
                applicationContext,
                "Q1BsLXpjZ1BsLXpjZ0FmUHRCREVDMENnQUJKQUFCSkFBQWlnSF9RRkFBRkFBY0FCQUFEUUFKZ0FVUUJBQUNPQUZhQU1vQXBvQlR3RFhBSjBBV0VBdk1CaklESndIR2dQLUFfNkFvQUFvQURnQUlBQWFBQk1BQ2lBSUFBUndBclFCbEFGTkFLZUFhNEJPZ0N3Z0Y1Z01aQVpPQTQwQl93QVpTQUNBYTRSQUJBTmNPZ0FnR3VBI18xXzEwXyNfX3MzMzFfczY3MV9zN19jMjcyMjRfczFfczI2X3M5MDVfczgwM19jMjc4NDJfczcyM19VXyMxLS0tIzF-MTMwMS4yNjA1Ljg5LjIyOS4yODc4LjI3MDc",
                callback = object : CmpImportCallback {
                    override fun onImportResult(success: Boolean, message: String) {
                        Log.d(TAG, "import successfully?: $message")
                    }
                }
            )
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }
        binding.buttonShowVendors.setOnClickListener {
            val res = consentTool.getEnabledVendorList(applicationContext)
            Log.d(TAG, "VendorList: $res")
            Toast.makeText(applicationContext, res.toString(), Toast.LENGTH_LONG).show()
        }

        binding.submitConsentVendor.setOnClickListener {
            val vendorId = binding.editTextVendorVendor.text
            val res = consentTool.hasVendorConsent(this, vendorId.toString(), checkConsent = true)
                .toString()
            Log.d(TAG, "hasVendorConsent: $res")
            Toast.makeText(this, res, Toast.LENGTH_SHORT).show()
        }

        binding.submitConsentVendorPurpose.setOnClickListener {
            val isIab: Boolean = binding.checkBoxIsIabVendorPurpose.isChecked
            val purposeId: String = binding.editTextVendorPurposePurpose.text.toString()
            val res = consentTool.hasPurposeConsent(this, purposeId, isIab).toString()
            Log.d(TAG, "hasVendorConsent: $res")
            Toast.makeText(this, res, Toast.LENGTH_SHORT).show()
        }

        binding.buttonShowCmpLayer.setOnClickListener {
            consentTool.openConsentLayer(this)
        }
    }

//    private fun addFragment() {
//        val fragmentManager : FragmentManager = supportFragmentManager
//        val fragmentTransaction : FragmentTransaction = fragmentManager.beginTransaction()
//        val s
//    }
    /**
     * Get Cmp Id as Int -> Future use of uuid as Strings
     */
    private fun getCmpId(): String {
        val cmpId: String = intent.getStringExtra("CMP_ID").toString()
        return if (cmpId != "") cmpId else DEFAULT_CMP_ID
    }

    private val actions = mapOf(
        "initialize" to Pair(
            { consentTool.initialize(this) },
            "Initialize the consent layer"
        ),
        "setCallbacks" to Pair(
            { consentTool.setCallbacks(null, null, null, null, null) },
            "Set callbacks"
        ),
        "checkAndOpenConsentLayer" to Pair(
            { consentTool.checkAndOpenConsentLayer(this) },
            "Check and open the consent layer"
        ),
        "openCustomLayerWithActivity" to Pair(
            { consentTool.openCustomLayer(this, R.id.cmpContainer) },
            "Open the customized consent layer (using activity)"
        ),
        "openCustomLayerWithContext" to Pair(
            { consentTool.openCustomLayer(this, object : CmpLayerAppEventListenerInterface {}) },
            "Open the customized consent layer (using context)"
        ),
        "openConsentLayer" to Pair(
            { consentTool.openConsentLayer(this) },
            "Open the consent layer"
        ),
        "createCustomLayerFragment" to Pair(
            { consentTool.createCustomLayerFragment(this) },
            "Create a CmpLayerFragment UI element"
        ),
        "hasConsent" to Pair(
            { consentTool.hasConsent() },
            "Check if the user gave consent"
        ),
        "getAllPurposesString" to Pair(
            { consentTool.getAllPurposes(this) },
            "Get all purposes as a string"
        ),
        "getAllPurposeList" to Pair(
            { consentTool.getAllPurposeList(this) },
            "Get all purposes as a list of strings"
        ),
        "getEnabledPurposes" to Pair(
            { consentTool.getEnabledPurposes(this) },
            "Get enabled purposes"
        ),
        "getEnabledPurposeList" to Pair(
            { consentTool.getEnabledPurposeList(this) },
            "Get enabled purpose list"
        ),
        "getDisabledPurposes" to Pair(
            { consentTool.getDisabledPurposes(this) },
            "Get disabled purposes"
        ),
        "getAllVendorsString" to Pair(
            { consentTool.getAllVendors(this) },
            "Get all vendors"
        ),
        "getAllVendorList" to Pair(
            { consentTool.getAllVendorsList(this) },
            "Get all vendors list"
        ),
        "getEnabledVendors" to Pair(
            { consentTool.getEnabledVendors(this) },
            "Get enabled vendors"
        ),
        "getEnabledVendorList" to Pair(
            { consentTool.getEnabledVendorList(this) },
            "Get enabled vendor list"
        ),
        "getDisabledVendors" to Pair(
            { consentTool.getDisabledVendors(this) },
            "Get disabled vendors"
        ),
        "enableVendorList" to Pair(
            { consentTool.enableVendorList(this, emptyList()) },
            "Enable vendor list"
        ),
        "disableVendorList" to Pair(
            { consentTool.disableVendorList(this, emptyList()) },
            "Disable vendor list"
        ),
        "enablePurposeList" to Pair(
            { consentTool.enablePurposeList(this, emptyList(), true) },
            "Enable purpose list"
        ),
        "disablePurposeList" to Pair(
            { consentTool.disablePurposeList(this, emptyList(), true) },
            "Disable purpose list"
        ),
    )


}
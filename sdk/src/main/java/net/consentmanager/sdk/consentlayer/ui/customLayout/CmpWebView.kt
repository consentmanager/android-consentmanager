package net.consentmanager.sdk.consentlayer.ui.customLayout

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import net.consentmanager.sdk.consentlayer.model.CMPConfig

private const val TAG = "CMP:WebView"

/**
 * Cmp web view
 *
 * @constructor
 *
 * @param context
 */
@SuppressLint("AddJavascriptInterface", "SetJavaScriptEnabled", "ViewConstructor")
internal class CmpWebView(context: Context) : WebView(context) {

    val js =
        "javascript:window.cmpToSDK_sendStatus = function(consent, jsonObject) { jsonObject.cmpString = consent; Android.onConsentReceived(consent, JSON.stringify(jsonObject)); };"
    val openJs =
        "javascript:window.cmpToSDK_showConsentLayer = function() { Android.onOpen();};"

    init {
        loadUrl("")
        settings.javaScriptEnabled = true
        webViewClient = CmpWebViewClient(context)
        // clear Data
        clearCache(true)
        clearHistory()
        clearFormData()

        // style  WebView
        layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        setBackgroundColor(Color.parseColor(CMPConfig.customLayerColor))
    }

    /**
     * Initialize
     *
     * @param cmpLayerAppInterface
     * @param url
     */
    fun initialize(cmpLayerAppInterface: CmpLayerAppInterface, url: String) {
        addJavascriptInterface(cmpLayerAppInterface, "Android")
        loadUrl(url)
    }

    /**
     * Change visibility
     *
     * @param visibility
     */
    fun changeVisibility(visibility: Int) {
        this.visibility = visibility
    }


    /**
     * Cmp web view client
     *
     * @property mContext
     * @constructor Create empty Cmp web view client
     */
    inner class CmpWebViewClient constructor(
        private val mContext: Context,
    ) : WebViewClient() {

        /**
         * On page started
         *
         * @param view
         * @param url
         * @param favicon
         */
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
        }

        /**
         * Should override url loading
         *
         * @param view
         * @param url
         * @return
         */
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return handleWebViewInteraction(url)
        }

        /**
         * Should override url loading
         *
         * @param view
         * @param request
         * @return
         */
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            return handleWebViewInteraction(request.url.toString())
        }

        private fun handleWebViewInteraction(url: String?): Boolean {
            if (url != null && url != "") {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                mContext.startActivity(browserIntent)
                return true
            }
            return false
        }

        /**
         * On load resource
         *
         * @param view
         * @param url
         */
        override fun onLoadResource(view: WebView?, url: String?) {
            view.run {
                loadUrl(js)
                loadUrl(openJs)
            }
            super.onLoadResource(view, url)

        }
    }

}
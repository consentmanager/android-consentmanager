package net.consentmanager.sdk.consentlayer.repository

import net.consentmanager.sdk.consentlayer.model.CmpConsent

internal interface ICmpRepository {

    fun saveConsentData(json: String)
    fun getConsentData(): CmpConsent
    fun removeConsentData()

    //State of Consent

    // writer
    fun saveLastConsentGiven()

    // getter
    fun getLastConsentGiven(): String?

    // removes all Data
    fun removeAll()
}
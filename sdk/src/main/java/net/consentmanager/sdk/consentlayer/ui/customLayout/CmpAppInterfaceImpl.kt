package net.consentmanager.sdk.consentlayer.ui.customLayout

import android.os.Handler
import android.os.Looper
import android.webkit.JavascriptInterface
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.CmpLayerAppEventListenerInterface
import net.consentmanager.sdk.consentlayer.model.CmpConsent
import net.consentmanager.sdk.consentlayer.service.CmpServiceInterface

/**
 * Cmp app interface impl
 *
 * @property fragment
 * @property cmpConsentService
 * @property cmpLayerAppEventListener
 * @constructor Create empty Cmp app interface impl
 */
internal class CmpAppInterfaceImpl(
    private val fragment: CmpLayerFragment?,
    private val cmpConsentService: CmpServiceInterface,
    private val cmpLayerAppEventListener: CmpLayerAppEventListenerInterface,
) : CmpLayerAppInterface {

    /**
     * On open
     *
     */
    @JavascriptInterface
    override fun onOpen() {
        cmpConsentService.onOpenAction()
        Handler(Looper.getMainLooper()).post {
            cmpLayerAppEventListener.onOpen(fragment)
        }
    }

    /**
     * On consent received
     *
     * @param consent
     * @param jsonObject
     */
    @JavascriptInterface
    override fun onConsentReceived(consent: String, jsonObject: String) {
        if (cmpConsentService.saveConsentJson(jsonObject)) {
            Handler(Looper.getMainLooper()).post {
                cmpLayerAppEventListener.onConsentReceived(
                    fragment,
                    CmpConsent.fromJson(jsonObject)
                )
            }
        } else if (fragment != null) {
            val fragmentManager = fragment.parentFragmentManager
            fragmentManager.beginTransaction().remove(fragment).commit()
        } else {
            cmpLayerAppEventListener.onError(null, CmpError.NetworkError)
        }
    }
}
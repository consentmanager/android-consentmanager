package net.consentmanager.sdk.consentlayer

internal object CmpState {

    private var state: State = State.INITIAL
    private var isLayerOpen: Boolean = false
    private var isLayerLoading: Boolean = false
    private var isRejectAllEvent: Boolean = false

    fun actionLayerOpened() {
        this.isLayerOpen = true
        this.isLayerLoading = false
        this.state = State.WAITING
    }

    fun actionLayerClosed() {
        this.isLayerLoading = false
        this.isLayerOpen = false
    }

    fun actionLayerLoading() {
        this.state = State.WAITING
        this.isLayerLoading = true
        this.isLayerOpen = false
    }

    fun rejectedAll() {
        this.isRejectAllEvent = true
        this.isLayerOpen = false
        this.state = State.WAITING
    }

    fun acceptedAll() {
        this.isLayerOpen = false
        this.state = State.WAITING
    }


    fun actionConsentReceived() {
        this.isLayerLoading = false
        this.state = State.READY
    }

    fun actionReset() {
        this.isLayerOpen = false
        this.state = State.INITIAL
    }

    fun isReady(): Boolean {
        return state == State.READY
    }

    fun isOpen(): Boolean {
        return isLayerOpen
    }

    fun isRejectAll(): Boolean {
        return this.isRejectAllEvent
    }

    fun isLayerLoading(): Boolean {
        return this.isLayerLoading
    }


}

enum class State {
    INITIAL,
    WAITING,
    READY
}

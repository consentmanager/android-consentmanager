package net.consentmanager.sdk.common.callbacks

/**
 * On open callback
 *
 */
interface OnOpenCallback {
    /**
     * On webView opened
     *
     */
    fun onWebViewOpened()
}
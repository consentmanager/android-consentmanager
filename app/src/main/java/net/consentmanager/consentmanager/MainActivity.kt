package net.consentmanager.consentmanager

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import net.consentmanager.consentmanager.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val defaultId: String = "201"
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        binding.submitConsentInit.setOnClickListener {
            submitConsentInit()
        }

        binding.languageSettings.setOnClickListener {
            goToLanguageSite()
        }
    }

    private fun submitConsentInit() {
        var id = binding.editTextCmpId.text.toString()
        if (id.isEmpty()) {
            id = defaultId
        }
        val intent = Intent(applicationContext, ConsentActivity::class.java).apply {
            putExtra("CMP_ID", id)
        }
        // TODO for import Consent and Ad id
//        if(consentString.isNotEmpty()) {
//            intent.putExtra("consent", consentString)
//        }
        startActivity(intent)
    }

    private fun goToLanguageSite() {
        val intent = Intent(applicationContext, NewLanguageActivity::class.java)
        startActivity(intent)
    }
}
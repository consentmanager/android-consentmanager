package net.consentmanager.sdk.consentlayer.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import net.consentmanager.sdk.consentlayer.model.valueObjects.CmpMetadata
import net.consentmanager.sdk.consentlayer.model.valueObjects.Purpose
import net.consentmanager.sdk.consentlayer.model.valueObjects.Vendor

/**
 * Cmp consent
 *
 * @property cmpStringBase64Encoded
 * @property googleAdditionalConsent
 * @property consentString
 * @property gdprApplies
 * @property googleVendorList
 * @property hasGlobalScope
 * @property publisherCC
 * @property regulation
 * @property regulationKey
 * @property tcfCompliant
 * @property tcfVersion
 * @property tcfcaVersion
 * @property gppVersions
 * @property uspString
 * @property allVendorsList
 * @property allPurposesList
 * @property enabledPurposesMap
 * @property enabledVendorsMap
 * @property enabledVendors
 * @property enabledPurposes
 * @property metadata
 * @property userChoiceExists
 * @property purModeActive
 * @property purModeLoggedIn
 * @property purModeLogic
 * @property consentExists
 * @constructor Create empty Cmp consent
 */
@Serializable
data class CmpConsent(
    @SerialName("cmpString") private val cmpStringBase64Encoded: String?,
    @SerialName("addtlConsent") val googleAdditionalConsent: String,
    @SerialName("consentstring") val consentString: String,
    @SerialName("gdprApplies") val gdprApplies: Boolean,
    @SerialName("googleVendorConsents") val googleVendorList: Map<String, Boolean>,
    @SerialName("hasGlobalScope") val hasGlobalScope: Boolean,
    @SerialName("publisherCC") val publisherCC: String,
    @SerialName("regulation") val regulation: Int,
    @SerialName("regulationKey") val regulationKey: String,
    @SerialName("tcfcompliant") val tcfCompliant: Boolean,
    @SerialName("tcfversion") val tcfVersion: Int,
    @SerialName("lastButtonEvent") val lastButtonEvent: Int = -1,
    @SerialName("tcfcaversion") val tcfcaVersion: String,
    @SerialName("gppversions") val gppVersions: List<String>?,
    @SerialName("uspstring") val uspString: String,
    @SerialName("vendorsList") private val allVendorsList: List<Vendor>,
    @SerialName("purposesList") private val allPurposesList: List<Purpose>,
    @SerialName("purposeLI") private val enabledPurposesMap: Map<String, Boolean>,
    @SerialName("vendorLI") private val enabledVendorsMap: Map<String, Boolean>,
    @SerialName("vendorConsents") private val enabledVendors: Map<String, Boolean>,
    @SerialName("purposeConsents") private val enabledPurposes: Map<String, Boolean>,
    @SerialName("metadata") private val metadata: List<CmpMetadata>,
    @SerialName("userChoiceExists") private val userChoiceExists: Boolean,
    @SerialName("purModeActive") val purModeActive: Boolean,
    @SerialName("purModeLoggedIn") val purModeLoggedIn: Boolean,
    @SerialName("purModeLogic") val purModeLogic: String,
    @SerialName("consentExists") val consentExists: Boolean,
) {
    companion object {
        fun fromJson(json: String): CmpConsent {
            try {
                val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
                return jsonConfig.decodeFromString(serializer(), json)
            } catch (e: Exception) {
                throw IllegalArgumentException("Error parsing JSON: ${e.message}")
            }
        }

        fun emptyConsent(): CmpConsent {
            return CmpConsent(
                cmpStringBase64Encoded = null,
                googleAdditionalConsent = "",
                consentString = "",
                gdprApplies = false,
                googleVendorList = emptyMap(),
                hasGlobalScope = false,
                publisherCC = "",
                regulation = 0,
                regulationKey = "",
                tcfCompliant = false,
                tcfVersion = 0,
                lastButtonEvent = 0,
                tcfcaVersion = "",
                gppVersions = emptyList(),
                uspString = "",
                allVendorsList = emptyList(),
                allPurposesList = emptyList(),
                enabledPurposesMap = emptyMap(),
                enabledVendorsMap = emptyMap(),
                enabledVendors = emptyMap(),
                enabledPurposes = emptyMap(),
                metadata = emptyList(),
                userChoiceExists = false,
                purModeActive = false,
                purModeLoggedIn = false,
                purModeLogic = "",
                consentExists = false
            )
        }
    }

    /**
     * Creates JSON String of the object
     * @return Json representation of the Consent Object
     */
    fun toJson(): String {
        try {
            val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
            return jsonConfig.encodeToJsonElement(this).toString()
        } catch (e: Exception) {
            // Handle the exception as appropriate for your use case
            throw IllegalArgumentException("Error serializing JSON: ${e.message}")
        }
    }

    /**
     * Has purpose
     *
     * @param id
     * @return
     */
    fun hasPurpose(id: String): Boolean {
        return enabledPurposes.containsKey(id.lowercase())
    }

    /**
     * Has vendor
     *
     * @param id
     * @return
     */
    fun hasVendor(id: String): Boolean {
        return enabledVendors.containsKey(id.lowercase())
    }

    /**
     * Get cmp id
     *
     * @return
     */
    fun getCmpStringBase64Encoded(): String {
        if (cmpStringBase64Encoded == null) {
            return ""
        }
        return cmpStringBase64Encoded
    }

    /**
     * Is valid
     *
     * @return
     */
    fun isValid(): Boolean {
        return consentString.isNotEmpty()
    }

    /**
     * Get purpose list
     *
     * @return
     */
    fun getAllPurposes(): List<String> {
        return allPurposesList.map { x -> x.id }
    }

    /**
     * Get agreed purposes
     *
     * @return
     */
    fun getEnabledPurposes(): List<String> {
        return enabledPurposes.keys.toList()
    }

    /**
     * Get disabled purposes
     *
     * @return
     */
    fun getDisabledPurposes(): List<String> {
        return getAllPurposes().subtract(getEnabledPurposes().toSet()).toList()
    }

    /**
     * Get disabled vendors
     *
     * @return
     */
    fun getDisabledVendors(): List<String> {
        return getAllVendor().subtract(getEnabledVendors().toSet()).toList()
    }


    /**
     * Get vendor list
     *
     * @return
     */
    fun getAllVendor(): List<String> {
        return allVendorsList.map { x -> (x.id) }
    }

    /**
     * Get agreed vendors
     *
     * @return
     */
    fun getEnabledVendors(): List<String> {
        return enabledVendors.map { x -> x.key }
    }


    /**
     * Is empty
     *
     * @return
     */
    fun isEmpty(): Boolean {
        return cmpStringBase64Encoded == "" || cmpStringBase64Encoded == null
    }

    /**
     * Get metadata
     *
     * @return
     */
    fun getMetadata(): List<CmpMetadata> {
        return metadata
    }


    /**
     * Has consent
     *
     * @return
     */
    fun hasConsent(): Boolean {
        return userChoiceExists
    }

    /**
     * To string
     *
     * @return
     */
    override fun toString(): String {
        return "CmpConsent(cmpId=$cmpStringBase64Encoded, addTlConsent='$googleAdditionalConsent', consentString='$consentString', gdprApplies=$gdprApplies, googleVendorList=$googleVendorList, hasGlobalScope=$hasGlobalScope, publisherCC='$publisherCC', regulation=$regulation, regulationKey='$regulationKey', tcfCompliant=$tcfCompliant, tcfVersion=$tcfVersion, uspString='$uspString', allVendorsList=$allVendorsList, purposeMap=$enabledPurposesMap, allPurposesList=$allPurposesList, agreedVendors=$enabledVendors, agreedPurposes=$enabledPurposes, metadata=$metadata, userChoiceExists=$userChoiceExists)"
    }

}
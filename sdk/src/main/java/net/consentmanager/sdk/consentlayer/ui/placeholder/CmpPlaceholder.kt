package net.consentmanager.sdk.consentlayer.ui.placeholder

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import net.consentmanager.sdk.common.callbacks.CmpPlaceholderEventListener
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import net.consentmanager.sdk.consentlayer.service.CmpConsentService

private const val TAG = "CMP:Placeholder"

private const val URL_PATTERN = "https://%s/delivery/apppreview.php?id=%s&vendor=%s"

private const val CONSENT_PREFIX = "consent://"

@Deprecated(message = "This class will be removed in the future, use the enable and disable Vendor API")
@SuppressLint("ViewConstructor")
class CmpPlaceholder(
    ctx: Context,
    cmpPlaceholderParams: CMPPlaceholderParams,
    cmpPlaceholderEventListener: CmpPlaceholderEventListener,
) : WebView(ctx) {

    private var cmpPlaceholderParams: CMPPlaceholderParams?
    private var cmpPlaceholderEventListener: CmpPlaceholderEventListener?
    private var config: CMPConfig = CMPConfig

    fun load(): CmpPlaceholder {
        val consent = CmpConsentService.getCmpConsent(context).getCmpStringBase64Encoded()
        loadUrl(getUrl(consent))
        return this
    }

    private fun getUrl(consent: String?): String {
        val url = StringBuilder(
            String.format(
                URL_PATTERN,
                config.serverDomain,
                config.id,
                cmpPlaceholderParams!!.vendorId
            )
        )
        // append optional Parameter if they exists else its just empty
        url.append(cmpPlaceholderParams!!.customPlaceholderToGetParam)
        url.append(cmpPlaceholderParams!!.imageUrlToGetParam)
        if (consent != null) {
            url.append(String.format("#cmpimport=%s", consent))
        }
        Log.d(TAG, String.format("Placeholder url of %s", url))

        return url.toString()
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun configureWebSettings() {
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        settings.defaultTextEncodingName = "utf-8"
    }

    override fun toString(): String {
        return "CMPPlaceholder{" +
                "cmpPlaceholderParams=" + cmpPlaceholderParams +
                ", config=" + config +
                '}'
    }

    inner class CmpWebViewClient(private var cmpPlaceholder: CmpPlaceholder) : WebViewClient() {

        @Deprecated("Deprecated in Java")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return handleOverrideUrlLoading(view, url)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?,
        ): Boolean {
            return handleOverrideUrlLoading(view, request?.url.toString())
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
        }

        private fun handleOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            Log.d(TAG, String.format("Web-view Interaction: %s", url))
            if (url == null) {
                return false
            }
            if (url == CONSENT_PREFIX) {
                Log.d(TAG, "Skip clicked, end instance")
                cmpPlaceholderEventListener?.errorOccurred("The Service is currently not Available")
                return true
            }
            return false
        }

    }

    companion object {
        @JvmStatic
        fun create(
            context: Context,
            cmpPlaceholderParams: CMPPlaceholderParams,
            cmpPlaceholderEventListener: CmpPlaceholderEventListener,
        ): CmpPlaceholder {
            return CmpPlaceholder(context, cmpPlaceholderParams, cmpPlaceholderEventListener)
        }
    }

    init {
        configureWebSettings()
        webViewClient = CmpWebViewClient(this)
        cmpPlaceholderParams.also { this.cmpPlaceholderParams = it }
        cmpPlaceholderEventListener.also { this.cmpPlaceholderEventListener = it }
    }
}

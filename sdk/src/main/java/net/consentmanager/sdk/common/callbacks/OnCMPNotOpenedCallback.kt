package net.consentmanager.sdk.common.callbacks

/**
 * On cmp not opened callback
 *
 */
interface OnCMPNotOpenedCallback {
    /**
     * Listener called when [CmpConsentLayerActivity] is finished after interacting with the WebView
     */
    fun onCMPNotOpened()
}
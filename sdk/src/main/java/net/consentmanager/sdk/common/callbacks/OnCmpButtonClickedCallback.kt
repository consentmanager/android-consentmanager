package net.consentmanager.sdk.common.callbacks

import net.consentmanager.sdk.consentlayer.model.valueObjects.CmpButtonEvent

/**
 * On cmp button clicked callback
 *
 * @constructor Create empty On cmp button clicked callback
 */
interface OnCmpButtonClickedCallback {
    /**
     * On button clicked
     *
     * @param event
     */
    fun onButtonClicked(event: CmpButtonEvent)
}
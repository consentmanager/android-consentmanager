package net.consentmanager.sdk.common.utils

import net.consentmanager.sdk.consentlayer.model.CMPConfig
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.Random

/**
 * Consent url builder
 *
 * @constructor Create empty Consent url builder
 */
internal class ConsentUrlBuilder {

    private var addVendors = ""
    private var id: String? = null
    private var serverDomain: String? = null
    private var idfa: String? = null
    private var language: String? = null
    private var appName: String? = null
    private var consent: String? = ""
    private var addPurposes = ""
    private var rejectAll = false
    private var acceptAll: Boolean = false
    private var designId: Int? = null
    private var skipToCustomizePage = false
    private var forceOpen = true

    private var debug = false

    /**
     * Build
     *
     * @return
     */
    fun build(): String {
        require(!(id == null || serverDomain == null)) { "You need to provide at least the app id and the specific consentmanager serverdomain" }
        val outcome = StringBuilder(String.format(URL_V4, serverDomain, id))
        if (debug) {
            outcome.append("&cmpdebug")
        }
        if (idfa != null) {
            outcome.append(String.format("&idfa=%s", idfa))
        }
        if (designId != null) {
            outcome.append(String.format("&usedesign=%s", designId))
        }
        if (language != null) {
            outcome.append(String.format("&cmplang=%s", language))
        }
        if (appName != null) {
            outcome.append(String.format("&appname=%s", appName))
        }
        if (consent != null && consent != "" && !rejectAll) {
            outcome.append(
                String.format(
                    "&zt=%s#cmpimport=%s",
                    dateAndRandomNumberAsString,
                    consent
                )
            )
        }
        if (skipToCustomizePage) {
            outcome.append("&cmpscreencustom")
        }
        if (forceOpen) {
            outcome.append("&cmpscreen")
        }
        if (acceptAll) {
            outcome.append("&cmpautoaccept=1&cmpscreen")
        }
        if (rejectAll) {
            outcome.append("&cmpautoreject=1&cmpscreen")
        }

        if (addVendors != "") {
            outcome.append("&cmpsetvendors=$addVendors")
        }

        if (addPurposes != "") {
            outcome.append("cmpsetpurposes=$addPurposes")
        }
        return outcome.toString()
    }

    /**
     * Set config
     *
     * @param config
     * @return
     */
    fun setConfig(config: CMPConfig): ConsentUrlBuilder {
        language = config.language
        appName = config.appName
        id = config.id
        idfa = config.idfa
        serverDomain = config.serverDomain
        skipToCustomizePage = config.skipToCustomizePage
        designId = config.designId
        debug = config.debug ?: false
        return this
    }

    /**
     * Set consent
     *
     * @param consent
     * @return
     */
    fun setConsent(consent: String?): ConsentUrlBuilder {
        if (consent == null) {
            return this
        }
        // don't set import value if reject all is set
        if (rejectAll) {
            return this
        }
        this.consent = consent
        return this
    }

    /**
     * Set force open
     *
     * @param force
     * @return
     */
    fun setForceOpen(force: Boolean): ConsentUrlBuilder {
        forceOpen = force
        return this
    }

    private val dateAndRandomNumberAsString: String
        get() {
            val rand = Random()
            val date = Calendar.getInstance().time
            val dateFormat: DateFormat = SimpleDateFormat("ddMMyyyy", Locale.ENGLISH)
            val strDate = dateFormat.format(date)
            return String.format(Locale.ENGLISH, "%s%d", strDate, rand.nextInt(10000))
        }

    /**
     * Set reject all
     *
     * @param rejectAll
     * @return
     */
    fun setRejectAll(rejectAll: Boolean): ConsentUrlBuilder {
        this.rejectAll = rejectAll
        //if reject all never set consent
        if (rejectAll) {
            consent = ""
        }
        return this
    }

    /**
     * Set vendors
     *
     * @param enable
     * @param vendors
     * @return
     */
    fun setVendors(enable: Boolean, vendors: List<String>): ConsentUrlBuilder {
        this.addVendors = vendors.joinToString(separator = "_")
        if (enable) {
            this.acceptAll = true
        } else {
            this.rejectAll = true
        }
        return this
    }

    /**
     * Set purposes
     *
     * @param enable
     * @param purposes
     * @param updateVendor
     * @return
     */
    fun setPurposes(
        enable: Boolean,
        purposes: List<String>,
        updateVendor: Boolean = true,
    ): ConsentUrlBuilder {
        this.addPurposes = purposes.joinToString(separator = "_")
        if (!updateVendor) {
            this.addPurposes = this.addPurposes + "&cmpdontfixpurposes"
        }
        if (enable) {
            this.acceptAll = true
        } else {
            this.rejectAll = true
        }
        return this
    }

    /**
     * Set accept all
     *
     * @param acceptAll
     * @return
     */
    fun setAcceptAll(acceptAll: Boolean): ConsentUrlBuilder {
        this.acceptAll = acceptAll
        if (acceptAll) {
            consent = ""
        }
        return this
    }

    companion object {
        private const val URL_V4 = "https://%s/delivery/appcmp_v4.php?cdid=%s"
    }
}
package net.consentmanager.sdk.common.callbacks

/**
 * Cmp import callback
 *
 */
interface CmpImportCallback {
    /**
     * On import result
     *
     * @param success Boolean if import was successfully
     * @param message String message
     */
    fun onImportResult(success: Boolean, message: String)
}
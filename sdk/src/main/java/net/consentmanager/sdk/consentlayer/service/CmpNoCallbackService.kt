package net.consentmanager.sdk.consentlayer.service

import android.util.Log
import net.consentmanager.sdk.consentlayer.model.CmpConsent

private const val TAG = "CMP:CmpService"

/**
 * Cmp no callback service
 *
 * @constructor Create empty Cmp no callback service
 */
class CmpNoCallbackService : CmpServiceInterface {
    /**
     * Save consent json
     *
     * @param cmpConsentJson
     * @return
     */
    override fun saveConsentJson(cmpConsentJson: String): Boolean {
        return true
    }

    /**
     * Save consent
     *
     * @param cmpConsent
     */
    override fun saveConsent(cmpConsent: CmpConsent) {
        Log.d(TAG, "no saving consent")
    }

    /**
     * On open action
     *
     */
    override fun onOpenAction() {
        Log.d(TAG, "Open signal")
    }

    /**
     * On close action
     *
     */
    override fun onCloseAction() {
        Log.d(TAG, "Close signal")
    }
}
package net.consentmanager.sdk.common.callbacks

import android.util.Log
import android.webkit.WebView

/**
 * Cmp placeholder event listener
 *
 * @constructor Create empty Cmp placeholder event listener
 */
interface CmpPlaceholderEventListener {
    /**
     * Vendor accepted
     *
     * @param view
     */
    fun vendorAccepted(view: WebView?) {
        Log.d(TAG, "Vendor Accepted")
    }

    /**
     * Consent updated
     *
     */
    fun consentUpdated() {
        Log.d(TAG, "Consent Updated: Vendor may be needed to accept again.")
    }

    /**
     * Error occurred
     *
     * @param message
     */
    fun errorOccurred(message: String?) {
        Log.d(TAG, String.format("Error Occurred: %s", message))
    }

    companion object {
        const val TAG = "CMP:Event"
    }
}
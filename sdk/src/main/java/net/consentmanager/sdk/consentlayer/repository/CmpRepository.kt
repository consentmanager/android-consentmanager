package net.consentmanager.sdk.consentlayer.repository

import android.content.Context
import androidx.annotation.Keep
import net.consentmanager.sdk.common.utils.EMPTY_DEFAULT_STRING
import net.consentmanager.sdk.consentlayer.service.CmpPreferences
import java.util.Date

private const val CMP_REQUEST = "IABConsent_CMPRequest"
private const val CMP_CHECKAPI_RESPONSE = "CMP_CHECKAPI_RESPONSE"
private const val CMP_CHECKAPI_LASTUPDATE = "CMP_CHECKAPI_LASTUPDATE"

@Keep
internal object CmpRepository {

    /**
     * Stores the passed date millis as a string in the shared preferences
     *
     * @param context Application Context
     * @param date the Date, when the Server was last requested
     */
    fun setLastRequested(context: Context, date: Date?) {
        val cmpPreferences = CmpPreferences(context)
        if (date != null) {
            cmpPreferences.setString(CMP_REQUEST, date.time.toString())
        } else {
            cmpPreferences.removeKey(CMP_REQUEST)
        }
    }

    /**
     * Returns the date when the server was requested for the last time
     *
     * @param context Application Context
     * @return the stored Date
     */
    fun getLastRequested(context: Context): Date? {
        val value = CmpPreferences(context).getString(CMP_REQUEST, EMPTY_DEFAULT_STRING)
        return if (value == EMPTY_DEFAULT_STRING) {
            null
        } else {
            Date(value.toLong())
        }
    }

    /**
     * Resets all Shared Preferences saved by this instance.
     * @param context Application Context
     */
    fun reset(context: Context) {
        val cmpPreferences = CmpPreferences(context)
        cmpPreferences.removeKey(CMP_REQUEST)
        cmpPreferences.removeKey(CMP_CHECKAPI_LASTUPDATE)
        cmpPreferences.removeKey(CMP_CHECKAPI_RESPONSE)
    }

    fun setCheckApiResponse(context: Context, value: Boolean) {
        val cmpPreferences = CmpPreferences(context)
        cmpPreferences.setBoolean(CMP_CHECKAPI_RESPONSE, value)
    }

    fun getLastCheckApiUpdate(context: Context): Date? {
        val cmpPreferences = CmpPreferences(context)
        return cmpPreferences.getDate(CMP_CHECKAPI_LASTUPDATE, null)
    }

    fun getCheckApiResponse(context: Context): Boolean {
        val cmpPreferences = CmpPreferences(context)
        return cmpPreferences.getBoolean(CMP_CHECKAPI_RESPONSE, false)
    }

    fun setCheckApiLastUpdate(context: Context) {
        val cmpPreferences = CmpPreferences(context)
        cmpPreferences.setDate(CMP_CHECKAPI_LASTUPDATE, Date())
    }
}
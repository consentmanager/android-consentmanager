package net.consentmanager.sdk.consentlayer.service

import net.consentmanager.sdk.consentlayer.model.CmpConsent

/**
 * Cmp service interface
 *
 * @constructor Create empty Cmp service interface
 */
interface CmpServiceInterface {
    /**
     * Save consent json
     *
     * @param cmpConsentJson
     * @return
     */
    fun saveConsentJson(cmpConsentJson: String): Boolean

    /**
     * Save consent
     *
     * @param cmpConsent
     */
    fun saveConsent(cmpConsent: CmpConsent)

    /**
     * On open action
     *
     */
    fun onOpenAction()

    /**
     * On close action
     *
     */
    fun onCloseAction()
}
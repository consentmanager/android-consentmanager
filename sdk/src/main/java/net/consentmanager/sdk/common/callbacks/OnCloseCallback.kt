package net.consentmanager.sdk.common.callbacks

/**
 * Provides a listener that will be called when [CmpConsentLayerActivity] is finished after interacting with the WebView
 */
interface OnCloseCallback {
    /**
     * Listener called when [CmpConsentLayerActivity] is finished after interacting with the WebView
     */
    fun onWebViewClosed()
}
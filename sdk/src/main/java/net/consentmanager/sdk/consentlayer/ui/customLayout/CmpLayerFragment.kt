package net.consentmanager.sdk.consentlayer.ui.customLayout

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.CmpLayerAppEventListenerInterface
import net.consentmanager.sdk.consentlayer.CmpState
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import net.consentmanager.sdk.consentlayer.service.CmpConsentService

private const val TAG = "CMP:FragmentView"

/**
 * Cmp layer fragment
 *
 * @constructor Create empty Cmp layer fragment
 */
class CmpLayerFragment : Fragment() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var cmpService: CmpConsentService
        internal lateinit var webView: CmpWebView

        internal fun create(
            context: Context,
            cmpConsentService: CmpConsentService,
        ): CmpLayerFragment {
            cmpService = cmpConsentService
            webView = CmpWebView(context)
            return CmpLayerFragment()
        }
    }

    /**
     *
     * On create view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return createLayout()
    }

    /**
     * On view created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        val linearLayout: LinearLayout = view as LinearLayout
        if (webView.parent != null) {
            (webView.parent as ViewGroup).removeView(webView) // <- fix
        }
        linearLayout.addView(webView)

        webView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
    }

    /**
     * initialize Fragment and load Cmp
     *
     * @param appInterface specific layer to app interface to take actions on events
     */
    fun initialize(appInterface: CmpLayerAppEventListenerInterface, url: String) {
        webView
            .initialize(
                CmpAppInterfaceImpl(
                    this,
                    cmpService,
                    appInterface
                ),
                url
            )
        cmpService.onLoadCmpLayer()
        object : CountDownTimer(CMPConfig.timeout.toLong(), 1000) {
            override fun onTick(p0: Long) {
                // Stays unimplemented
            }

            override fun onFinish() {
                if (CmpState.isLayerLoading()) {
                    cmpService.onErrorOccurred(
                        CmpError.TimeoutError,
                        "Network error. The layer has problems to open View. Please try again later"
                    )
                    Log.d(
                        TAG,
                        "The CMP Layer has problems to open View: Please try again later"
                    )
                    appInterface.onError(this@CmpLayerFragment, CmpError.TimeoutError)
                }
            }
        }.start()
    }

    private fun createLayout(): LinearLayout {
        // programmatically add a Linear layout webView
        val linearLayout = LinearLayout(context)
        linearLayout.layoutParams =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
        return linearLayout
    }

    /**
     * On destroy view
     *
     */
    override fun onDestroyView() {
        webView.parent?.let {
            (it as ViewGroup).removeView(webView)
        }
        webView.clearCache(true)
        webView.clearHistory()
        webView.destroy()
        super.onDestroyView()
    }
}
package net.consentmanager.sdk.consentlayer.model.valueObjects

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
class Purpose(
    @SerialName("id")
    val id: String
) {
    override fun toString(): String {
        return "Purpose(id='$id')"
    }
}
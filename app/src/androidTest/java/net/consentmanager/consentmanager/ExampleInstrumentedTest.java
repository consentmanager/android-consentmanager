package net.consentmanager.consentmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import net.consentmanager.sdk.CMPConsentTool;
import net.consentmanager.sdk.common.callbacks.OnCloseCallback;
import net.consentmanager.sdk.common.callbacks.OnOpenCallback;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("net.consentmanager.consentmanager", appContext.getPackageName());
    }
    @Test
    public void testAllConstructor() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("net.consentmanager.consentmanager", appContext.getPackageName());

        CMPConsentTool tool = CMPConsentTool.Companion.createInstance(appContext, 12, "test.com", "test", "DE");
        assertNotNull("1 Cmp Instantiated", tool);
         tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", (OnCloseCallback) () -> {});
        assertNotNull("2 Cmp Instantiated", tool);
        tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", () -> {}, () -> {});
        assertNotNull("3 Cmp Instantiated", tool);
        tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", 1000, () -> {}, () -> {});
        assertNotNull("4 Cmp Instantiated", tool);
        tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", (OnCloseCallback) () -> {});
        assertNotNull("5 Cmp Instantiated", tool);
        tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", (OnCloseCallback) () -> {});
        assertNotNull("6 Cmp Instantiated", tool);
        tool = CMPConsentTool.Companion.createInstance(appContext, 12, "", "", "", (OnCloseCallback) () -> {});
        assertNotNull("7 Cmp Instantiated", tool);

        // Basic Data should be
        // id, domain and appName, lang
        //additional builder pattern
    }

}
package net.consentmanager.sdk.consentlayer.model.valueObjects

/**
 * Gdpr status
 *
 * @property subject
 * @constructor Create empty Gdpr status
 */
enum class GdprStatus(val subject: String) {
    /**
     * Gdpr Unknown
     *
     * @constructor Create empty Gdpr Unknown
     */
    GDPR_UNKNOWN("-1"),

    /**
     * Gdpr Disabled
     *
     * @constructor Create empty Gdpr Disabled
     */
    GDPR_DISABLED("0"),

    /**
     * Gdpr Enabled
     *
     * @constructor Create empty Gdpr Enabled
     */
    GDPR_ENABLED("1");

    companion object {
        private val map = GdprStatus.values().associateBy(GdprStatus::subject)

        @JvmStatic
        fun fromString(s: String) = map[s]
    }
}
package net.consentmanager.sdk.common

/**
 * Cmp error
 *
 */
sealed class CmpError {
    object NetworkError : CmpError()
    object TimeoutError : CmpError()
    object ConsentReadWriteError : CmpError()
    object ConsentLayerError : CmpError()
}
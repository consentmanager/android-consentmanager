# Changelog

All notable changes to this project will be documented in this file.

## Version 1.6.3 (Released on 2023-05-10)

### Added

- callback OnConsentReceived for all APIS to change vendor and purpose
- support for older Android Versions

## Version 1.6.2 (Released on 2023-04-19)

### Added

- Added types for metadata keys.

## Version 1.6.1 (Released on 2023-04-18)

### Added

- Check API with onOpenCallback
- Added consentServiceinterface and no callback service.

### Refactored

- Fixed cmp error on different locations
- Updated Gradle version
- Set default value for button event.

## Version 1.6.0 (Released on 2023-03-17)

### Added

- Config object improved and added reset
- Added missing interface functions

### Refactored

- Improved stability for CMPConfig
- Removed Metadata as internal class
- Improved stability and updated data of Cmp Consent object
- Improved stability for invalid json decoding and encoding
- Logging improved and refactored unused functions
- Change API signature of checkAndOpenCmpLayer, added optional appInterface
- Change API signature getLastConsentString to getConsentstring, exportConsentString to
  exportCmpString, importCMPData to importCmpString, to differentiate more between the cmp string by
  consentmanager and the consentstring by iab
- Change API signature getAgreedVendor becomes getEnabledVendors
- Upgraded gradle to 7.4.1
- Improved stability for errors while consentlayer is open
- Improved stability for migration and decoding and encoding of newer and older api
- Improved service class and error handling

### Fixed

- Fixed idfa string
- possible memory leak and improved performance

### Removed

- Deprecation note for placeholder API
- Unused listener
- Internalized Repository
- Internalized classes for module

#### Test

- Refactored test classes
- Updated tests
- Added test for consent object
- Added constructor tests

## Version 1.5.7 (Released on 2023-02-16)

### Features

- Added get list of disabled purposes and vendors
- Disabled purposes and vendors

### Fixes

- Missing errorCallback in createInstance

### Refactor

- Improved stability by adding try catch blocks around serialization and deserialization parts
- Network availability checks improved
- Refactored API and added interface. Cleaned endpoints and marked specific endpoints as deprecated
- Improved stability for saving consent

### Style

- Updated docs and cleaned code
- Documentation

## Version 1.5.6 (Released on 2023-02-07)

### Changes

- Resolved a bug related to GDPR or CCPA.

## Version 1.5.5 (Released on 2023-02-07)

### Fixes

- Eliminated the Java class for improved performance.

## Version 1.5.4 (Released on 2023-02-02)

### Refactored

- Huge refactoring and changing API of hasVendor

## Version 1.5.3 (Released on 2023-02-02)

### Fixes

- Removed static instance of service

## Version 1.5.2 (Released on 2023-01-24)

### Fixes

- Removed static instance of service

## Version 1.5.1 (Released on 2023-01-23)

### Refactored

- Refactoring and cleanup

## Version 1.5.0 (Released on 2023-01-19)

### Changes

- Added API getAgreed and getAll Purposes and Vendors

## Version 1.4.94 (Released on 2023-01-06)

### Changes

- Added static callback class

## Version 1.4.93 (Released on 2023-01-05)

### Changes

- Added vendor and purpose list API

## Version 1.4.92 (Released on 2023-01-04)

### Changes

- Added on error callback and CMP language API

## Version 1.4.91 (Released on 2023-01-03)

### Fixes

- Removed unnecessary log

## Version 1.4.9 (Released on 2022-12-28)

### Changes

- Added common error

## Version 1.4.8 (Released on 2022-12-16)

### Changes

- Updated Android Material

## Version 1.5.6 (Released on 2023-02-07)

### Changes

- Resolved a bug related to GDPR or CCPA.

## Version 1.5.5 (Released on 2023-02-07)

### Fixes

- Eliminated the Java class for improved performance.

## Version 1.4.8 (Released on 2022-12-16)

### Changes

- Updated Android Material.

## Version 1.4.7 (Released on 2022-12-13)

### Changes

- Added hasConsent.

## Version 1.4.6 (Released on 2022-12-13)

### Fixes

- Fixed a missing key error.

## Version 1.4.5 (Released on 2022-12-13)

### Changes

- Added the feature "rejectAll" and "hasConsentPurpose/Vendor" feature.

## Version 1.4.4 (Released on 2022-11-29)

### Changes

- Added a javascript event.

## Version 1.4.3 (Released on 2022-11-29)

### Changes

- Added activity styles.

## Version 1.4.2 (Released on 2022-11-10)

### Changes

- Updated the CMP SDK version.

## Version 1.4.1 (Released on 2022-11-01)

### Changes

- Initialize.

## Version 1.4.0 (Released on 2022-10-31)

### Changes

- Implemented fragment and cleanups.

## Version 1.3.4 (Released on 2022-10-05)

### Added

- feature custom color and custom screen size

## Version 1.3.3 (Released on 2022-04-08)

### Added

- events after save

## Version 1.3.2 (Released on 2022-04-08)

### Added

- consent saved event after save function

## Version 1.3.1 (Released on 2022-04-08)

### Added

- activity new task

## Version 1.3.0 (Released on 2020-08-04)

### Fixed

- Check for consent if empty or null

## Version 1.2.9 (Released on 2020-07-04)

### Changed

- Additional Verification on hasPurpose/hasVendor to check if consent is given

## Version 1.2.8 (Released on 2022-07-04)

### Fixed

- Event triggers for closing CMP fixed
- Event trigger for not opened CMP fixed. Not open event will now be triggered also when
  ConsentLayer pushes a consent without opening.

## Version 1.2.7 (Released on 2022-27-03)

### Changed

- OpenListener callback moved to the actual layer opening event

## Version 1.2.6 (Released on 2022-15-03)

### Refactored

- Stability improvements for CMP Communication of ConsentLayer
- Refactored UI Activity and WebView

### Fixed

- ConsentLayer user consent synchronization to SDK fixed

## Version 1.2.5 (Released on 2022-13-03)

### Refactored

- Stability for CCPA/GDP Appliances
- Reliability of network requests

## Version 1.2.4 (Released on 2022-20-02)

### Fixed

- Possibility to createInstance without any callback functions
- Fixed Status of CCPA/GDPR Appliances on vendor/purpose requests

## Version 1.2.3 (Released on 2022-02-02)

### Fixed

- Fixed Bug with need acceptance behavior

### Changed

- Activity is running in background

## Version 1.2.2 (Released on 2022-30-01)

### Changed

- changed time of inserting js functions on webview
- backend API call changed if no consent is present

## Version 1.2.1 (Released on 2021-12-02)

### Changed

- changed API call of ConsentLayer
- Visibility

## Version 1.2.0 (Released on 2021-11-10)

### Added

- Placeholder Preview Object

### Refactored

- introduced kotlin objects for future upcoming Android Sdk
- synced Event callback names between projects

### Fixed

- fixed issue of reopening ConsentLayer

## Version 1.1.0 (Released on 2021-08-24)

### Fixed

- deactivated dismissing the CMP modal
- handle consent logic with non-EU traffic

### Refactored

- changed URL generator logic

## Version 1.0.1 (Released on 2021-06-22)

### Fixed

- fixed minified bug

## Version 1.0.0 (Released on 2021-03-26)

### Added

- added AddId Property handling
- added Readme and working example project

## Version 0.0.1 (Released on 2021-03-24)

### Added

- Providing integration through jitpack


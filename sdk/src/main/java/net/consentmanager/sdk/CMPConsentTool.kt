package net.consentmanager.sdk

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.widget.Button
import androidx.annotation.IdRes
import androidx.annotation.Keep
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.*
import net.consentmanager.sdk.common.utils.*
import net.consentmanager.sdk.consentlayer.CmpState
import net.consentmanager.sdk.consentlayer.model.*
import net.consentmanager.sdk.consentlayer.model.valueObjects.RegulationStatus
import net.consentmanager.sdk.consentlayer.repository.CmpRepository
import net.consentmanager.sdk.consentlayer.service.CmpConsentService
import net.consentmanager.sdk.consentlayer.service.CmpConsentService.Companion.getCmpConsent
import net.consentmanager.sdk.consentlayer.service.CmpNoCallbackService
import net.consentmanager.sdk.consentlayer.ui.consentLayer.CmpConsentLayerActivity
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpAppInterfaceImpl
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpLayerFragment
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpWebView
import net.consentmanager.sdk.consentlayer.ui.placeholder.CMPPlaceholder
import net.consentmanager.sdk.consentlayer.ui.placeholder.CMPPlaceholderParams
import net.consentmanager.sdk.consentlayer.ui.placeholder.CmpPlaceholder
import java.text.SimpleDateFormat
import java.util.*

private const val TXT_CONSENTLAYER_NOT_SHOWN = "Layer can not be opened, fragment not available!"

private const val TXT_NO_INTERNET_CONNECTION = "No internet connection"

private const val TXT_CONSENT_RECEIVED = "Consent Received"

private const val TAG = "CMP:Manager"

/**
 * Flag to ignore State conditions
 */
private const val IGNORE_STATE = false

/**
 * This is the Main class providing all functionalities of the CMP Consent Tool.
 * You have to initialise this class with the method createInstance(...). The config
 * data needs to be send via the method or been declared in the AndroidManifest of this module.
 *
 * @property config CMPConfig for configuring the CMPConsentTool
 * @constructor
 * private constructor which creates the instance of the CMPConsentTool
 *
 * @param context Application Context
 * @param closeListener CloseListener to add action when the consentlayer closed
 * @param openListener OpenListener to add action when the consentlayer opens
 * @param cmpNotOpenedCallback NotOpenedListener to add action when the consentlayer does not need to be opened
 * @param onErrorCallback ErrorCallback to add action if an error occurred
 */
@Keep
class CMPConsentTool private constructor(
    val context: Context,
    var config: CMPConfig,
    closeListener: OnCloseCallback? = null,
    openListener: OnOpenCallback? = null,
    cmpNotOpenedCallback: OnCMPNotOpenedCallback? = null,
    onErrorCallback: OnErrorCallback? = null,
    onCmpButtonClickedCallback: OnCmpButtonClickedCallback? = null,
) : CmpManagerInterface {


    /**
     * Initialize consent layer which lead to open the layer eventually
     *
     * @param appInterface
     * @return
     */
    override fun initialize(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface?,
    ): CMPConsentTool {
        checkAndOpenConsentLayer(context, appInterface)
        return this
    }

    private val cmpService: CmpConsentService
    private val callbackWrapper: CmpCallbackWrapper?


    init {
        callbackWrapper = CmpCallbackWrapper.withCloseCallback(closeListener)
            .withOpenCallback(openListener)
            .withNotOpenCallback(cmpNotOpenedCallback)
            .withErrorCallback(onErrorCallback)
            .withButtonClickedCallback(onCmpButtonClickedCallback)
        cmpService = CmpConsentService(context).setCallbacks(callbackWrapper)
    }

    /**
     * Set callbacks
     *
     * @param openListener
     * @param closeListener
     * @param cmpNotOpenedCallback
     * @param onErrorCallback
     * @param onCmpButtonClickedCallback
     */
    override fun setCallbacks(
        openListener: OnOpenCallback?,
        closeListener: OnCloseCallback?,
        cmpNotOpenedCallback: OnCMPNotOpenedCallback?,
        onErrorCallback: OnErrorCallback?,
        onCmpButtonClickedCallback: OnCmpButtonClickedCallback?,
    ) {
        CmpCallbackWrapper.withCloseCallback(closeListener)
            .withOpenCallback(openListener)
            .withNotOpenCallback(cmpNotOpenedCallback)
            .withErrorCallback(onErrorCallback)
            .withButtonClickedCallback(onCmpButtonClickedCallback)
    }

    /**
     * Update config
     *
     * @param config
     */
    fun updateConfig(config: CMPConfig) {
        Log.v(TAG, "updating config, $config")
        this.config = config
    }


    companion object {

        /**
         * If the ConsentManager was initialised
         * return If the ConsentManager was initialised
         */
        private fun isConsentToolInitialized(): Boolean {
            return instance != null
        }

        /**
         * Returns the CMPConsentTool. If you have not initialised the CMPConsentTool before,
         * The CMPConsentToolInitialisation Exception will be thrown.
         *
         * @return the initialised singleton Instant of the consent Manager.
         */
        private var instance: CMPConsentTool? = null

        /**
         * Initialises the CMPConsentTool with the given context and a given Config. You need to
         * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
         * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
         * make your code more readable and to have all your configs on one place.
         *
         * @param context Application Context
         * @param codeId Cmp Id
         * @param serverDomain Cmp server domain
         * @param appName application name
         * @param lang language for the consentlayer
         * @param idfa Android Advertising Id
         * @param timeout timeout for consentlayer
         * @param openListener OpenListener to add action on consentlayer opens
         * @param closeListener closeListener to add action on consentlayer closed
         * @param cmpNotOpenedCallback notOpenedCallback to add action when consentlayer does not open
         * @param errorCallback errorCallback to add action if an error occurred
         * @return
         */
        @Throws(IllegalStateException::class)
        @JvmOverloads
        @JvmStatic
        fun createInstance(
            context: Context,
            codeId: String,
            serverDomain: String?,
            appName: String?,
            lang: String?,
            idfa: String? = null,
            timeout: Int = DEFAULT_TIMEOUT,
            openListener: OnOpenCallback? = null,
            closeListener: OnCloseCallback? = null,
            cmpNotOpenedCallback: OnCMPNotOpenedCallback? = null,
            errorCallback: OnErrorCallback? = null,
            cmpButtonClickedCallback: OnCmpButtonClickedCallback? = null,
        ): CMPConsentTool {

            CMPConfig.apply {
                this.id = codeId
                this.serverDomain = serverDomain
                this.appName = appName
                this.language = lang
                this.idfa = idfa
                this.timeout = timeout
            }
            // Check if the config values are valid
            if (!CMPConfig.isValid()) {
                throw IllegalStateException("CMPConfig values are not valid")
            }

            if (isConsentToolInitialized()) {
                instance?.apply {
                    updateConfig(CMPConfig)
                    setCallbacks(
                        openListener = openListener,
                        closeListener = closeListener,
                        cmpNotOpenedCallback = cmpNotOpenedCallback,
                        onErrorCallback = errorCallback,
                        onCmpButtonClickedCallback = cmpButtonClickedCallback
                    )
                }
            } else {
                instance = CMPConsentTool(
                    context,
                    CMPConfig,
                    openListener = openListener,
                    closeListener = closeListener,
                    cmpNotOpenedCallback = cmpNotOpenedCallback,
                    onErrorCallback = errorCallback,
                    onCmpButtonClickedCallback = cmpButtonClickedCallback
                )
            }
            return requireNotNull(instance)
        }

        /**
         * Initialises the CMPConsentTool with the given context and a given Config. You need to
         * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
         * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
         * make your code more readable and to have all your configs on one place.
         *
         * @param context Application Context
         * @param config CMPConfig instance
         * @param openListener OpenListener to add action on consentlayer opens
         * @param closeListener closeListener to add action on consentlayer closed
         * @param cmpNotOpenedCallback notOpenedCallback to add action when consentlayer does not open
         * @param errorCallback errorCallback to add action if an error occurred
         * @return The created singleton Consent Manager Instance
         */
        @Throws(IllegalStateException::class)
        @JvmOverloads
        @JvmStatic
        fun createInstance(
            context: Context,
            config: CMPConfig,
            openListener: OnOpenCallback? = null,
            closeListener: OnCloseCallback? = null,
            cmpNotOpenedCallback: OnCMPNotOpenedCallback? = null,
            errorCallback: OnErrorCallback? = null,
            cmpButtonClickedCallback: OnCmpButtonClickedCallback? = null,
        ): CMPConsentTool {
            // Check if the config values are valid
            if (!config.isValid()) {
                throw IllegalStateException("CMPConfig values are not valid")
            }

            if (isConsentToolInitialized()) {
                instance?.apply {
                    updateConfig(CMPConfig)
                    setCallbacks(
                        openListener = openListener,
                        closeListener = closeListener,
                        cmpNotOpenedCallback = cmpNotOpenedCallback,
                        onErrorCallback = errorCallback,
                        onCmpButtonClickedCallback = cmpButtonClickedCallback
                    )
                }
            } else {
                instance = CMPConsentTool(
                    context,
                    CMPConfig,
                    openListener = openListener,
                    closeListener = closeListener,
                    cmpNotOpenedCallback = cmpNotOpenedCallback,
                    onErrorCallback = errorCallback,
                    onCmpButtonClickedCallback = cmpButtonClickedCallback
                )
            }
            return requireNotNull(instance)
        }

        /**
         * Gets the created instance of the CMPConsentTool. If not available the function
         * tries to create an instance.
         *
         * @param context Application Context
         * @param config CMPConfig
         * @return instance of CMPConsentTool
         */
        fun getInstance(context: Context, config: CMPConfig? = null): CMPConsentTool {
            if (instance == null) {
                instance = createInstance(context, config ?: CMPConfig)
            }
            return requireNotNull(instance)
        }

        /**
         * An import Method that imports the full consentString generated by a ConsentWebView
         * into the Shared Preferences of this device.
         *
         * @param context Application Context
         * @param cmpStringBase64Encoded The String that should be set, encoded in base64
         * @param callback The callback to receive the result of the import operation
         */
        fun importCmpString(
            context: Context,
            cmpStringBase64Encoded: String,
            callback: CmpImportCallback,
        ) {
            if (!isNetworkAvailable(context)) {
                Log.d(TAG, TXT_NO_INTERNET_CONNECTION)
                callback.onImportResult(false, "No internet connection")
                return
            }

            val url = getCmpRequestUrl(context, cmpStringBase64Encoded = cmpStringBase64Encoded)
            Log.v(TAG, "Import Cmp Url: $url")
            val cmpService = CmpConsentService(context)
            try {
                val webView = CmpWebView(context)
                webView.initialize(
                    CmpAppInterfaceImpl(
                        null,
                        cmpService,
                        object : CmpLayerAppEventListenerInterface {
                            override fun onOpen(fragment: CmpLayerFragment?) {
                                callback.onImportResult(
                                    false,
                                    "The consent string could not be imported."
                                )
                            }

                            override fun onConsentReceived(
                                fragment: CmpLayerFragment?,
                                cmpConsent: CmpConsent,
                            ) {
                                callback.onImportResult(
                                    true,
                                    "The consent string was imported successfully."
                                )
                            }
                        }), url
                )
            } catch (e: Exception) {
                callback.onImportResult(
                    false,
                    e.message ?: "An unknown error occurred while importing the consent string."
                )
            }
        }


        /**
         * An export Function that can be called to export the full consentString generated by a ConsentWebView
         * and saved in the Shared Preferences of this device.
         *
         * @param context Application Context
         */
        fun exportCmpString(context: Context): String {
            val cmpService = CmpConsentService(context)
            Log.v(TAG, "CMP export Cmp String: ${cmpService.getCmpString()}")
            return cmpService.getCmpString()
        }

        fun reset(context: Context) {
            Log.v(TAG, "Clearing all values")
            CmpConsentService.resetAll(context)
        }

        /**
         * sets the instance manually
         *
         * @param instance CMPConsentTool?
         */
        internal fun setInstance(instance: CMPConsentTool?) {
            this.instance = instance
        }

        /**
         * gets the status of the Consent
         *
         * @param context Application Context
         */
        internal fun getStatus(context: Context) {
            Log.d(
                TAG, getCmpConsent(context).toJson()
            )
        }

        /**
         *
         * creates a placeholder View
         *
         * @param context Application Context
         * @param cmpPlaceholderParams placeholder parameter
         * @param cmpPlaceholderEventListener CmpPlaceholderEventListener to control View object
         * @return CMPPlaceholder View
         */
        @Deprecated(
            message = "This function will be removed in the future, please use the enable/disable Vendor API",
            replaceWith = ReplaceWith(
                "consentTool.enableVendorList(listOf(/'123/'))"
            )
        )
        fun createPlaceholder(
            context: Context,
            cmpPlaceholderParams: CMPPlaceholderParams?,
            cmpPlaceholderEventListener: CmpPlaceholderEventListener?,
        ): CMPPlaceholder {
            return CMPPlaceholder.create(
                context,
                cmpPlaceholderParams,
                cmpPlaceholderEventListener
            )
        }
    }


    /**
     * Set open cmp consent tool view listener
     * @deprecated
     * @param context Application Context
     * @param gdprButton Button UI object
     * @param callback Callback on close
     */
    override fun setOpenCmpConsentToolViewListener(
        context: Context,
        gdprButton: Button,
        callback: OnCloseCallback?,
    ) {
        gdprButton.setOnClickListener {
            this@CMPConsentTool.openCmpConsentToolView(
                context
            )
        }
        Log.v(TAG, "OpenCmpConsentToolViewListener initialised")
    }

    /**
     * Create custom layer fragment
     *
     * @param activity FragmentActivity
     * @return CmpLayerFragment UI Element
     */
    override fun createCustomLayerFragment(activity: FragmentActivity): CmpLayerFragment {
        return CmpLayerFragment.create(activity.applicationContext, cmpService)
    }

    /**
     * Sets a Listener to the given button, If the Button is clicked, a modal view will be displayed
     * with the consent web view. If the Compliance is accepted or rejected, a close function will be
     * called. You can override this close function with your own. Therefor implement the OnCloseCallback
     * and add this as an other parameter.
     *
     * @param context Application Context
     * @param gdprButton The Button, the openCmpConsentToolViewListener should be added to.
     */
    fun setOpenCmpConsentToolViewListener(context: Context, gdprButton: Button) {
        gdprButton.setOnClickListener {
            this@CMPConsentTool.openCmpConsentToolView(
                context
            )
        }
    }

    /**
     * opens the Consent layer
     *
     * @param context Application Context
     */
    override fun openConsentLayer(context: Context) {
        val url = getCmpRequestUrl(forceOpen = true)
        if (!hasNetworkConnection(context)) {
            return
        }
        Log.v(TAG, "Opening Consentlayer with URL: $url")
        if (CMPConfig.isCustomLayer) {
            openCustomLayer(
                context as (FragmentActivity),
                CMPConfig.containerViewId,
            )
        } else {
            CmpConsentLayerActivity.openCmpConsentToolView(
                context,
                url
            )
        }
    }

    /**
     * Set open place holder view listener
     *
     * @param context Application Context
     * @param actionButton Button UI Element
     * @param vendor Vendor ID
     */
    @Deprecated(message = "This function will be removed in the future")
    override fun setOpenPlaceHolderViewListener(
        context: Context,
        actionButton: Button,
        vendor: String,
    ) {
        Log.d(TAG, "CMP call deprecated setOpenPlaceholderViewListener")
        actionButton.setOnClickListener {
            CmpPlaceholder(
                context,
                CMPPlaceholderParams.ofVendor(vendor),
                object : CmpPlaceholderEventListener {})
        }
    }

    /**
     * Called this day
     *
     * @param context Application Context
     *
     * @deprecated This function will be removed in the future.
     * @return Boolean if the layer was called today
     */
    override fun calledThisDay(context: Context): Boolean {
        Log.v(TAG, "Last Consentlayer call: ${getCalledLast(context)}")
        val last = getCalledLast(context)
        if (last != null) {
            val now = Date()
            @SuppressLint("SimpleDateFormat") val formatter = SimpleDateFormat("dd-MM-yyyy")
            return formatter.format(now) == formatter.format(last)
        }
        return false
    }

    /**
     * Needs acceptance
     *
     * @param context Application Context
     * @return Boolean if the Cmp needs a user consent
     */
    override fun needsAcceptance(context: Context): Boolean {
        // need acceptance != hasConsent
        Log.v(TAG, "Need acceptance: ${!cmpService.getCmpConsent().hasConsent()}")
        return !cmpService.getCmpConsent().hasConsent()
    }

    /**
     * Get all purposes
     *
     * @param context Application Context
     * @return String of all existing purpose ids separated by _
     */
    override fun getAllPurposes(context: Context): String {
        return cmpService.getCmpConsent().getAllPurposes().joinToString(separator = "_")
    }

    /**
     * Get all purpose list
     *
    @param context Application Context
     * @return List of all existing purpose ids
     */
    override fun getAllPurposeList(context: Context): List<String> {
        return cmpService.getCmpConsent().getAllPurposes()
    }

    /**
     * Get agreed purposes
     *
     * @param context Application Context
     * @return List of agreed Purposes separated by _
     */
    override fun getEnabledPurposes(context: Context): String {
        return cmpService.getCmpConsent().getEnabledPurposes().joinToString(separator = "_")
    }

    /**
     * Get agreed purpose list
     *
     * @param context Application Context
     * @return List of agreed Purposes
     */
    override fun getEnabledPurposeList(context: Context): List<String> {
        return cmpService.getCmpConsent().getEnabledPurposes()
    }

    /**
     * get disabled purposes
     *
     * @param context Application Context
     * @return List of disabled purpose ids
     */
    override fun getDisabledPurposes(context: Context): List<String> {
        return cmpService.getCmpConsent().getDisabledPurposes()
    }

    /**
     * get disabled vendors
     *
     * @param context Application context
     * @return List of disabled vendor ids
     */
    override fun getDisabledVendors(context: Context): List<String> {
        return cmpService.getCmpConsent().getDisabledVendors()
    }

    /**
     * Get all vendors
     *
     * @param context Application Context
     * @return String of all vendor ids separated by _
     */
    override fun getAllVendors(context: Context): String {
        return cmpService.getCmpConsent().getAllVendor().joinToString(separator = "_")
    }

    /**
     * Get all vendors list
     *
     * @param context Application Context
     * @return List of all vendor ids
     */
    override fun getAllVendorsList(context: Context): List<String> {
        return cmpService.getCmpConsent().getAllVendor()
    }

    /**
     * Get agreed vendors
     *
     * @param context Application Context
     * @return String of the agreed vendors separated by _
     */
    override fun getEnabledVendors(context: Context): String {
        return cmpService.getCmpConsent().getEnabledVendors().joinToString(separator = "_")
    }

    /**
     * Get agreed vendor list
     *
     * @param context Application Context
     * @return List of the agreed vendors
     */
    override fun getEnabledVendorList(context: Context): List<String> {
        return cmpService.getCmpConsent().getEnabledVendors()
    }


    /**
     * Get US privacy string
     *
     * @param context Application Context
     * @return The us privacy String
     */
    override fun getUSPrivacyString(context: Context): String {
        return cmpService.getCmpConsent().uspString
    }


    /**
     * Get google a c string
     *
     * @param context Application Context
     * @return The googleAC String
     */
    override fun getGoogleACString(context: Context): String {
        return cmpService.getCmpConsent().googleAdditionalConsent
    }

    /**
     * Has vendor consent
     *
     * @param context Application Context
     * @param id vendor id
     * @param checkConsent check Consent if no consent available
     * @return if the user gave consent to the given id
     */
    override fun hasVendorConsent(
        context: Context,
        id: String,
        checkConsent: Boolean,
    ): Boolean {

        var consent = cmpService.getCmpConsent()

        if (!checkConsent) {
            return consent.hasVendor(id)
        }

        val regulationStatus = cmpService.getRegulationStatus()

        if (consent.isEmpty()) {
            Log.v(TAG, "No Consent available. CMP will eventually open Consentlayer")
            openConsentLayerEventually(context)
            consent = cmpService.getCmpConsent()
        }

        // If unknown the ccpa or gdpr not applied
        if (checkRegulationStatusIsUnknown(regulationStatus) && consent.hasConsent()) {
            Log.d(TAG, "GDPR OR CCPA not applied!")
            return true
        }

        return consent.hasVendor(id)
    }

    /**
     * Has purpose consent
     *
     * @param context Application Context
     * @param id purpose id
     * @param isIABPurpose is an iab purpose or not
     * @param checkConsent check Consent if no consent available
     * @return if the user gave consent to the given id
     */
    override fun hasPurposeConsent(
        context: Context,
        id: String,
        isIABPurpose: Boolean,
        checkConsent: Boolean,
    ): Boolean {
        var consent = cmpService.getCmpConsent()

        if (!checkConsent) {
            Log.v(TAG, "No Consent available. CMP will eventually open Consentlayer")
            return consent.hasPurpose(id)
        }
        val regulationStatus = cmpService.getRegulationStatus()

        if (consent.isEmpty()) {
            openConsentLayerEventually(context)
            consent = cmpService.getCmpConsent()
        }

        // If unknown the
        if (checkRegulationStatusIsUnknown(regulationStatus) && consent.hasConsent()) {
            Log.d(TAG, "GDPR OR CCPA not applied!")
            return true
        }
        return consent.hasPurpose(id)
    }

    /**
     * Get called last
     *
     * @param context Application Context
     * @return Date of the last received consent
     */
    override fun getCalledLast(context: Context): Date? {
        return CmpRepository.getLastRequested(context)
    }

    /**
     * Get last consent string
     *
     * @param context Application Context
     * @return String of the last given consent
     */
    override fun getConsentstring(context: Context): String {
        return cmpService.getCmpString()
    }

    /**
     * Export Consentstring
     *
     * @return String of the consent
     */
    override fun exportCmpString(): String {
        return cmpService.getCmpString()
    }

    override fun importCmpString(cmpString: String, importCallback: CmpImportCallback) {
        if (!isNetworkAvailable(context)) {
            Log.d(TAG, TXT_NO_INTERNET_CONNECTION)
            importCallback.onImportResult(false, "No internet connection")
            return
        }

        val url = getCmpRequestUrl(context, cmpStringBase64Encoded = cmpString)
        Log.d(TAG, "Import Cmp URL: $url")
        try {
            val webView = CmpWebView(context)
            webView.initialize(
                CmpAppInterfaceImpl(
                    null,
                    cmpService,
                    object : CmpLayerAppEventListenerInterface {
                        override fun onOpen(fragment: CmpLayerFragment?) {
                            importCallback.onImportResult(
                                false,
                                "The consent string could not be imported."
                            )
                        }

                        override fun onConsentReceived(
                            fragment: CmpLayerFragment?,
                            cmpConsent: CmpConsent,
                        ) {
                            importCallback.onImportResult(
                                true,
                                "The consent string was imported successfully."
                            )
                        }
                    }), url
            )
        } catch (e: Exception) {
            importCallback.onImportResult(
                false,
                e.message ?: "An unknown error occurred while importing the consent string."
            )
        }
    }

    /**
     * Has consent
     *
     * @return if the user has consent or not
     */
    override fun hasConsent(): Boolean {
        return cmpService.getCmpConsent().hasConsent()
    }

    /**
     * check state and opens consentLayer eventually
     *
     * @param context Application Context
     */
    override fun checkAndOpenConsentLayer(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface?,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        if (!calledThisDay(context) || IGNORE_STATE) {
            openConsentLayerEventually(context, appInterface)
        } else {
            cmpService.handleCalledToday(context)
        }
    }

    /**
     * Check
     *
     * @param context
     * @param onCmpLayerOpenCallback
     * @param isCached
     */
    override fun check(
        context: Context,
        onCmpLayerOpenCallback: OnCmpLayerOpenCallback,
        isCached: Boolean,
    ) {
        val url = getCmpRequestUrl()
        if (isCached) {
            val lastRequest: Date? = cmpService.lastCheckApiUpdate()
            Log.v(TAG, "Cache active, last live request was ${lastRequest}.")
            if (isDateToday(lastRequest)) {
                if (cmpService.getCheckApiResponse()) {
                    onCmpLayerOpenCallback.onOpen()
                    return
                }
                return
            }
        }
        cmpService.saveCheckApiNeedOpenLayer(false)
        val webView = CmpWebView(context = context)
        webView.initialize(
            CmpAppInterfaceImpl(
                null,
                CmpNoCallbackService(),
                object : CmpLayerAppEventListenerInterface {
                    override fun onOpen(fragment: CmpLayerFragment?) {
                        onCmpLayerOpenCallback.onOpen()
                        cmpService.saveCheckApiNeedOpenLayer(true)
                    }
                }), url
        )
    }

    /**
     * Open custom layer
     *
     * @param activity
     * @param containerViewId
     */
    override fun openCustomLayer(
        activity: FragmentActivity,
        @IdRes containerViewId: Int,
    ) {
        if (!hasNetworkConnection(activity)) {
            return
        }
        val url = getCmpRequestUrl(forceOpen = true)
        val fragment = CmpLayerFragment.create(activity.applicationContext, cmpService)
        fragment.initialize(appInterface = createAppInterface(activity, containerViewId), url = url)
    }

    /**
     * Open custom layer
     *
     * @param context Application Context
     * @param appInterface CmpLayerAppEventListenerInterface for customized fragment control
     */
    override fun openCustomLayer(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        val url = getCmpRequestUrl(forceOpen = true)
        val fragment = CmpLayerFragment.create(context, cmpService)
        fragment.initialize(
            appInterface,
            url
        )
    }

    /**
     * Enable vendor list
     *
     * @param context Application Context
     * @param vendors List of vendor ids to enable
     */
    override fun enableVendorList(
        context: Context,
        vendors: List<String>,
        onConsentReceivedCallback: OnConsentReceivedCallback?,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        val url =
            ConsentUrlBuilder().setConfig(CMPConfig)
                .setConsent(cmpService.getCmpString()).setForceOpen(false)
                .setVendors(true, vendors)
                .build()
        Log.v(TAG, "Enabling Vendorlist with URL: $url")
        requestConsentLayer(
            context,
            onConsentReceivedCallback ?: object : OnConsentReceivedCallback {
                override fun onFinish() {
                    Log.d(TAG, TXT_CONSENT_RECEIVED)
                }
            },
            url
        )
    }

    /**
     * Disable vendor list
     *
     * @param context Application Context
     * @param vendors List of vendor ids to disable
     */
    override fun disableVendorList(
        context: Context,
        vendors: List<String>,
        onConsentReceivedCallback: OnConsentReceivedCallback?,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        val url =
            ConsentUrlBuilder().setConfig(CMPConfig)
                .setConsent(cmpService.getCmpString()).setForceOpen(false)
                .setVendors(false, vendors)
                .build()
        Log.v(TAG, "Disabling VendorList with URL: $url")
        requestConsentLayer(
            context,
            onConsentReceivedCallback ?: object : OnConsentReceivedCallback {
                override fun onFinish() {
                    Log.d(TAG, TXT_CONSENT_RECEIVED)
                }
            },
            url
        )
    }

    /**
     * Enable purpose list
     *
     * @param context Application Context
     * @param purposes List of Purpose ids to enable
     * @param updateVendor automatically enables depending vendors of the purpose
     */
    override fun enablePurposeList(
        context: Context,
        purposes: List<String>,
        updateVendor: Boolean,
        onConsentReceivedCallback: OnConsentReceivedCallback?,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        val url = ConsentUrlBuilder().setConfig(CMPConfig)
            .setConsent(cmpService.getCmpString()).setForceOpen(false)
            .setPurposes(true, purposes, updateVendor).build()
        Log.v(TAG, "Enabling PurposeList with URL: $url")
        requestConsentLayer(
            context,
            onConsentReceivedCallback ?: object : OnConsentReceivedCallback {
                override fun onFinish() {
                    Log.d(TAG, TXT_CONSENT_RECEIVED)
                }
            },
            url
        )
    }

    /**
     * Disable purpose list
     *
     * @param context Application Context
     * @param purposes List of Purpose ids to disable
     * @param updateVendor automatically enables depending vendors of the purpose
     */
    override fun disablePurposeList(
        context: Context,
        purposes: List<String>,
        updateVendor: Boolean,
        onConsentReceivedCallback: OnConsentReceivedCallback?,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        val url = ConsentUrlBuilder().setConfig(CMPConfig)
            .setConsent(cmpService.getCmpString()).setForceOpen(false)
            .setPurposes(false, purposes, updateVendor).build()
        Log.v(TAG, "Disabling PurposeList with URL: $url")
        requestConsentLayer(
            context,
            onConsentReceivedCallback ?: object : OnConsentReceivedCallback {
                override fun onFinish() {
                    Log.d(TAG, TXT_CONSENT_RECEIVED)
                }
            },
            url
        )
    }


    /**
     * Reject all
     *
     * @param context Application Context
     * @param onConsentReceivedCallback Callback when new Consent is received
     */
    override fun rejectAll(context: Context, onConsentReceivedCallback: OnConsentReceivedCallback) {
        if (!hasNetworkConnection(context)) {
            return
        }
        CmpState.rejectedAll()
        val url = getCmpRequestUrl(rejectAll = true)
        requestConsentLayer(context, onConsentReceivedCallback, url)
    }

    /**
     * Accept all
     *
     * @param context Application Context
     * @param onConsentReceivedCallback Callback when new Consent is received
     */
    override fun acceptAll(context: Context, onConsentReceivedCallback: OnConsentReceivedCallback) {
        if (!hasNetworkConnection(context)) {
            return
        }
        CmpState.acceptedAll()
        val url = getCmpRequestUrl(acceptAll = true)
        requestConsentLayer(context, onConsentReceivedCallback, url)
    }

    private fun requestConsentLayer(
        context: Context,
        onConsentReceivedCallback: OnConsentReceivedCallback,
        url: String,
    ) {
        val webView = CmpWebView(context = context)
        webView.initialize(
            CmpAppInterfaceImpl(
                null,
                cmpService,
                object : CmpLayerAppEventListenerInterface {

                    override fun onConsentReceived(
                        fragment: CmpLayerFragment?,
                        cmpConsent: CmpConsent,
                    ) {
                        Log.d(TAG, "consent Received for reject all")
                        onConsentReceivedCallback.onFinish()
                    }
                }), url
        )
    }

    /**
     * checks if internet connection is available and calls error callback if not
     *
     * @param context Application Context
     * @return has internet connection
     */
    private fun hasNetworkConnection(context: Context): Boolean {
        if (!isNetworkAvailable(context)) {
            Log.d(TAG, TXT_NO_INTERNET_CONNECTION)
            cmpService.onErrorOccurred(CmpError.NetworkError, TXT_NO_INTERNET_CONNECTION)
            return false
        }
        return true
    }

    /**
     * Creating the AppInterface to interact with the consentlayer
     *
     * @param activity
     * @return
     */
    private fun createAppInterface(
        activity: FragmentActivity,
        @IdRes containerViewId: Int,
    ): CmpLayerAppEventListenerInterface {
        return object : CmpLayerAppEventListenerInterface {
            override fun onOpen(fragment: CmpLayerFragment?) {
                if (fragment == null) {
                    cmpService.onErrorOccurred(
                        CmpError.ConsentLayerError,
                        TXT_CONSENTLAYER_NOT_SHOWN
                    )
                    return
                }
                startFragmentTransaction(activity, containerViewId, fragment)
            }

            override fun onConsentReceived(
                fragment: CmpLayerFragment?,
                cmpConsent: CmpConsent,
            ) {
                if (fragment == null) {
                    cmpService.onErrorOccurred(
                        CmpError.ConsentLayerError,
                        TXT_CONSENTLAYER_NOT_SHOWN
                    )
                    return
                }
                closeFragment(activity, fragment)
            }

            override fun onError(fragment: CmpLayerFragment?, error: CmpError) {
                Log.e(TAG, error.toString())
                if (fragment == null) {
                    cmpService.onErrorOccurred(
                        CmpError.ConsentLayerError,
                        "Error opening custom layer, please try again later."
                    )
                    return
                }
                closeFragment(activity, fragment)
            }
        }
    }

    private fun closeFragment(activity: FragmentActivity, fragment: CmpLayerFragment) {
        val ft = activity.supportFragmentManager
        ft.popBackStackImmediate()
        ft.beginTransaction().remove(fragment).commit()
    }

    private fun startFragmentTransaction(
        activity: FragmentActivity,
        @IdRes containerViewId: Int,
        fragment: CmpLayerFragment,
    ) {
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(containerViewId, fragment).show(fragment)
        transaction.commit()
    }

    /**
     * generates the cmp request URL
     *
     * @param forceOpen
     * @param acceptAll
     * @param rejectAll
     * @return
     */
    private fun getCmpRequestUrl(
        forceOpen: Boolean = false,
        acceptAll: Boolean = false,
        rejectAll: Boolean = false,
    ): String {
        val consentString = cmpService.getCmpString()
        return ConsentUrlBuilder()
            .setConfig(CMPConfig)
            .setConsent(consentString)
            .setForceOpen(forceOpen)
            .setRejectAll(rejectAll)
            .setAcceptAll(acceptAll)
            .build()
    }

    /**
     * checks the regulation Status
     *
     * @param regulationStatus RegulationStatus
     * @return if the regulation status is unknown
     */
    private fun checkRegulationStatusIsUnknown(
        regulationStatus: RegulationStatus?,
    ): Boolean {
        return regulationStatus === RegulationStatus.UNKNOWN
    }

    /**
     * verifies if consent layer has to be open
     *
     * @param context Application Context
     */
    private fun openConsentLayerEventually(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface? = null,
    ) {
        if (!hasNetworkConnection(context)) {
            return
        }
        if (config.isCustomLayer && appInterface != null) {
            openCustomLayer(context, appInterface)
        } else if (config.isCustomLayer) {
            openCustomLayer(context as FragmentActivity, config.containerViewId)
        } else {
            CmpConsentLayerActivity.openCmpConsentToolView(context, getCmpRequestUrl())
        }
    }

    /**
     * Open cmp consent tool view
     *
     * @param context Application Context
     */
    fun openCmpConsentToolView(context: Context) {
        openConsentLayer(context)
    }
}
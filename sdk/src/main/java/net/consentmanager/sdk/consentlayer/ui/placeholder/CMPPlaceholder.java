package net.consentmanager.sdk.consentlayer.ui.placeholder;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import net.consentmanager.sdk.common.callbacks.CmpPlaceholderEventListener;
import net.consentmanager.sdk.consentlayer.model.CMPConfig;
import net.consentmanager.sdk.consentlayer.service.CmpConsentService;

@Deprecated()
@SuppressLint("ViewConstructor")
public class CMPPlaceholder extends WebView {

    private static final String TAG = "CMP:Placeholder";

    private static final String URL_PATTERN = "https://%s/delivery/apppreview.php?id=%s&vendor=%s";

    private final CMPPlaceholderParams cmpPlaceholderParams;

    private final CmpPlaceholderEventListener cmpPlaceholderEventListener;

    private final CMPConfig config;

    CMPPlaceholder(Context context, CMPPlaceholderParams cmpPlaceholderParams, CmpPlaceholderEventListener cmpPlaceholderEventListener) {
        super(context);
        config = CMPConfig.INSTANCE;
        this.cmpPlaceholderEventListener = cmpPlaceholderEventListener;
        this.cmpPlaceholderParams = cmpPlaceholderParams;
        this.setWebViewClient(new CMPWebViewClient(this));
        WebSettings webSettings = this.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setSupportZoom(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    public static CMPPlaceholder create(Context context, CMPPlaceholderParams cmpPlaceholderParams, CmpPlaceholderEventListener cmpPlaceholderEventListener) {
        return new CMPPlaceholder(context, cmpPlaceholderParams, cmpPlaceholderEventListener);
    }

    public void show() {
        String consent = CmpConsentService.Companion.getCmpConsent(getContext()).getConsentString();
        loadUrl(getPlaceholderUrl(consent));
    }

    private String getPlaceholderUrl(String consent) {
        StringBuilder url = new StringBuilder(String.format(URL_PATTERN, config.getServerDomain(), config.getId(), cmpPlaceholderParams.getVendorId()));
        // append optional Parameter if they exists else its just empty
        url.append(cmpPlaceholderParams.getCustomPlaceholderToGetParam());
        url.append(cmpPlaceholderParams.getImageUrlToGetParam());
        if (consent != null) {
            url.append(String.format("#cmpimport=%s", consent));
        }
        Log.d(TAG, String.format("Placeholder url of %s", url));

        return url.toString();
    }

    @NonNull
    @Override
    public String toString() {
        return "CMPPlaceholder{" +
                "cmpPlaceholderParams=" + cmpPlaceholderParams +
                ", config=" + config +
                '}';
    }

    private class CMPWebViewClient extends WebViewClient {

        private static final String TAG = "CMP:Webview";

        private static final String CONSENT_PREFIX = "consent://";

        private final CMPPlaceholder placeholder;

        public CMPWebViewClient(CMPPlaceholder placeholder) {
            this.placeholder = placeholder;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return handleWebViewInteraction(view, url);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return handleWebViewInteraction(view, String.valueOf(request.getUrl()));
        }

        private boolean handleWebViewInteraction(WebView view, String url) {
            Log.d(TAG, String.format("Webview Interaction: %s", url));
            if (url == null) {
                return false;
            }
            if (url.equals(CONSENT_PREFIX)) {
                Log.d(TAG, "Skip clicked, end instance");
                cmpPlaceholderEventListener.errorOccurred("The Service is currently not Available");
                return true;
            }
            return false;
        }
    }
}




package net.consentmanager.sdk.consentlayer.ui.customLayout

import android.webkit.JavascriptInterface

/**
 * Cmp layer app interface
 *
 * @constructor Create empty Cmp layer app interface
 */
interface CmpLayerAppInterface {
    /**
     * On open
     *
     */
    @JavascriptInterface
    fun onOpen()

    /**
     * On consent received
     *
     * @param consent
     * @param jsonObject
     */
    @JavascriptInterface
    fun onConsentReceived(consent: String, jsonObject: String)
}
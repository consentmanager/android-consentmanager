package net.consentmanager.sdk.common.callbacks

import android.util.Log
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.consentlayer.model.CmpConsent
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpLayerFragment

private const val TAG = "CMP:AppInterface"

/**
 * Cmp layer app event listener interface
 *
 */
interface CmpLayerAppEventListenerInterface {

    /**
     * On open
     *
     */
    fun onOpen(fragment: CmpLayerFragment?) {
        Log.d(CALLBACK_TAG, "open Signal")
    }

    /**
     * On consent received
     *
     * @param cmpConsent
     */
    fun onConsentReceived(fragment: CmpLayerFragment?, cmpConsent: CmpConsent) {
        Log.d(CALLBACK_TAG, "consent Received")
    }


    fun onError(fragment: CmpLayerFragment?, error: CmpError) {
        Log.d(CALLBACK_TAG, "timeout reached")
        Log.e(CALLBACK_TAG, error.toString())
    }

}
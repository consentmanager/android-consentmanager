package net.consentmanager.sdk.consentlayer.model.valueObjects

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Cmp metadata object
 * Stores dynamically data to the platform localstorage
 *
 * @property key
 * @property value
 * @property type
 * @constructor Create empty Cmp metadata
 */

@Serializable
class CmpMetadata(
    @SerialName("name")
    val key: String? = "",
    @SerialName("value")
    val value: String? = "",
    @SerialName("type")
    val type: String? = "string",
)
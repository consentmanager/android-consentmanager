package net.consentmanager.sdk.consentlayer.ui.consentLayer

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi

/**
 * Cmp web view client
 *
 * @property mContext
 * @constructor Create empty Cmp web view client
 */
internal class CmpWebViewClient constructor(
    private val mContext: Context,
) : WebViewClient() {

    private val js =
        "javascript:window.cmpToSDK_sendStatus = function(consent, jsonObject) { jsonObject.cmpString = consent; Android.onConsentReceived(consent, JSON.stringify(jsonObject)); };"
    private val openJs =
        "javascript:window.cmpToSDK_showConsentLayer = function() { Android.onOpen();};"

    /**
     * Should override url loading
     *
     * @param view
     * @param url
     * @return
     */
    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        return handleWebViewInteraction(url)
    }

    /**
     * Should override url loading
     *
     * @param view
     * @param request
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        return handleWebViewInteraction(request.url.toString())
    }

    private fun handleWebViewInteraction(url: String?): Boolean {
        if (url != null && url != "") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mContext.startActivity(browserIntent)
            return true
        }
        return false
    }

    /**
     * On load resource
     *
     * @param view
     * @param url
     */
    override fun onLoadResource(view: WebView?, url: String?) {
        view?.run {
            loadUrl(js)
            loadUrl(openJs)
        }
        super.onLoadResource(view, url)

    }
}
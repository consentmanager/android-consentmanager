package net.consentmanager.sdk.common.callbacks

import android.util.Log
import net.consentmanager.sdk.common.CmpError


/**
 * On error callback
 *
 */
interface OnErrorCallback {

    /**
     * Error occurred
     *
     * @param type CmpError type
     * @param message String Error message
     */
    fun errorOccurred(type: CmpError, message: String) {
        Log.i("CMP:Error", message)
    }
}
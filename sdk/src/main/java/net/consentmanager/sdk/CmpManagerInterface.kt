package net.consentmanager.sdk

import android.content.Context
import android.widget.Button
import androidx.fragment.app.FragmentActivity
import net.consentmanager.sdk.common.callbacks.CmpImportCallback
import net.consentmanager.sdk.common.callbacks.CmpLayerAppEventListenerInterface
import net.consentmanager.sdk.common.callbacks.OnCMPNotOpenedCallback
import net.consentmanager.sdk.common.callbacks.OnCloseCallback
import net.consentmanager.sdk.common.callbacks.OnCmpButtonClickedCallback
import net.consentmanager.sdk.common.callbacks.OnCmpLayerOpenCallback
import net.consentmanager.sdk.common.callbacks.OnConsentReceivedCallback
import net.consentmanager.sdk.common.callbacks.OnErrorCallback
import net.consentmanager.sdk.common.callbacks.OnOpenCallback
import net.consentmanager.sdk.consentlayer.ui.customLayout.CmpLayerFragment
import java.util.Date

interface CmpManagerInterface {

    /**
     * Initialize
     *
     * @param context
     * @param appInterface
     * @return
     */
    fun initialize(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface? = null,
    ): CmpManagerInterface

    /**
     * Set callbacks
     *
     * @param openListener
     * @param closeListener
     * @param cmpNotOpenedCallback
     * @param onErrorCallback
     * @param onCmpButtonClickedCallback
     */
    fun setCallbacks(
        openListener: OnOpenCallback?,
        closeListener: OnCloseCallback?,
        cmpNotOpenedCallback: OnCMPNotOpenedCallback?,
        onErrorCallback: OnErrorCallback?,
        onCmpButtonClickedCallback: OnCmpButtonClickedCallback?,
    )

    /**
     * Check and open consent layer
     *
     * @param context Application Context
     * @param appInterface Custom AppInterface
     */
    fun checkAndOpenConsentLayer(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface? = null,
    )

    /**
     * opens the customized consent layer
     *
     * @param activity instance
     * @param containerViewId Int container UI id
     */
    fun openCustomLayer(
        activity: FragmentActivity,
        containerViewId: Int,
    )

    /**
     * Checks if the layer needs to be open, the check API will request the live system through and synchronizes the data with the backend.
     *
     * @param context Application Context
     * @param onCmpLayerOpenCallback Callback if the layer needs to be open
     * @param isCached caching option. If enabled it will cache the response of the backend for the current day.
     */
    fun check(
        context: Context,
        onCmpLayerOpenCallback: OnCmpLayerOpenCallback,
        isCached: Boolean = false,
    )

    /**
     * opens the customized consent layer
     *
     * @param context Application Context
     * @param appInterface implementation of the CmpLayerAppEventListenerInterface
     */
    fun openCustomLayer(
        context: Context,
        appInterface: CmpLayerAppEventListenerInterface,
    )

    /**
     * opens the consent layer
     *
     * @param context Application Context
     */
    fun openConsentLayer(context: Context)

    /**
     * Create custom layer fragment
     *
     * @param activity Activity
     * @return CmpLayerFragment
     */
    fun createCustomLayerFragment(activity: FragmentActivity): CmpLayerFragment

    /**
     * check if the user gave consent
     *
     * @return app has consent or not
     */
    fun hasConsent(): Boolean

    /**
     * returns all purposes as a String
     *
     * @param context Application Context
     * @return purpose String
     */
    fun getAllPurposes(context: Context): String

    /**
     * returns all purposes as a List of String
     *
     * @param context Application Context
     * @return purpose List<String>
     */
    fun getAllPurposeList(context: Context): List<String>


    /**
     * Get agreed purposes
     *
     * @param context Application Context
     * @return
     */
    fun getEnabledPurposes(context: Context): String

    /**
     * Get agreed purpose list
     *
     * @param context Application Context
     * @return
     */
    fun getEnabledPurposeList(context: Context): List<String>

    /**
     * Get disabled purposes
     *
     * @param context Application Context
     * @return
     */
    fun getDisabledPurposes(context: Context): List<String>

    /**
     * Get all vendors
     *
     * @param context Application Context
     * @return
     */
    fun getAllVendors(context: Context): String

    /**
     * Get all vendors list
     *
     * @param context Application Context
     * @return
     */
    fun getAllVendorsList(context: Context): List<String>

    /**
     * Get agreed vendors
     *
     * @param context Application Context
     * @return
     */
    fun getEnabledVendors(context: Context): String

    /**
     * Get agreed vendor list
     *
     * @param context Application Context
     * @return
     */
    fun getEnabledVendorList(context: Context): List<String>

    /**
     * Get disabled vendors
     *
     * @param context Application Context
     * @return
     */
    fun getDisabledVendors(context: Context): List<String>

    /**
     * Enable vendor list
     *
     * @param context Application Context
     * @param vendors
     * @param onConsentReceivedCallback
     */
    fun enableVendorList(
        context: Context,
        vendors: List<String>,
        onConsentReceivedCallback: OnConsentReceivedCallback? = null,
    )

    /**
     * Disable vendor list
     *
     * @param context Application Context
     * @param vendors  List of vendor ids to enable.
     * @param onConsentReceivedCallback Optional callback for handling consent received event.
     *                                  If provided, it will be called when consent is received.
     *                                  If not provided (null), a default callback implementation will be used.
     *                                  The default callback logs a message when consent is received.
     */
    fun disableVendorList(
        context: Context,
        vendors: List<String>,
        onConsentReceivedCallback: OnConsentReceivedCallback? = null,
    )

    /**
     * Enable purpose list
     *
     * @param context Application Context
     * @param purposes List of purpose ids to enable.
     * @param updateVendor flag if true also enable corresponding vendors
     * @param onConsentReceivedCallback  Optional callback for handling consent received event.
     *                                   If provided, it will be called when consent is received.
     *                                   If not provided (null), a default callback implementation will be used.
     *                                   The default callback logs a message when consent is received.
     */
    fun enablePurposeList(
        context: Context,
        purposes: List<String>,
        updateVendor: Boolean = true,
        onConsentReceivedCallback: OnConsentReceivedCallback? = null,
    )

    /**
     * Disable a list of purposes
     *
     * @param context Application Context
     * @param purposes List of purposes ids to disable
     * @param updateVendor flag if true also disable corresponding vendors
     * @param onConsentReceivedCallback  Optional callback for handling consent received event.
     *                                   If provided, it will be called when consent is received.
     *                                   If not provided (null), a default callback implementation will be used.
     *                                   The default callback logs a message when consent is received.
     */
    fun disablePurposeList(
        context: Context,
        purposes: List<String>,
        updateVendor: Boolean = true,
        onConsentReceivedCallback: OnConsentReceivedCallback? = null,
    )

    /**
     * Rejects the the consent layer and behaves the same when the user did not accept the consent
     *
     * @param context Application Context
     * @param onConsentReceivedCallback Callback for handling consent received event.
     *                                  it will be called when consent is received.
     */
    fun rejectAll(context: Context, onConsentReceivedCallback: OnConsentReceivedCallback)

    /**
     * Accept all
     *
     * @param context Application Context
     * @param onConsentReceivedCallback Callback for handling consent received event.
     *                                  it will be called when consent is received.
     */
    fun acceptAll(context: Context, onConsentReceivedCallback: OnConsentReceivedCallback)

    /**
     * Get u s privacy string
     *
     * @param context  Application Context
     * @return US Privacy String
     */
    fun getUSPrivacyString(context: Context): String

    /**
     * Has vendor consent
     *
     * @param context Application Context
     * @param id vendor id
     * @param checkConsent if true check for an given consent
     * @return Boolean if consent has given vendor
     */
    fun hasVendorConsent(context: Context, id: String, checkConsent: Boolean = true): Boolean

    /**
     * Has purpose consent
     *
     * @param context Application Context
     * @param id purpose id
     * @param isIABPurpose (deprecated) if purpose is an IAB Purpose
     * @param checkConsent if true check for an given consent
     * @return
     */
    fun hasPurposeConsent(
        context: Context,
        id: String,
        isIABPurpose: Boolean,
        checkConsent: Boolean = true,
    ): Boolean

    /**
     * Get Google AC string
     *
     * @param context Application Context
     * @return Google AC String
     */
    fun getGoogleACString(context: Context): String

    /**
     * Get last consent string
     *
     * @param context Application Context
     * @return saved Consent String
     */
    fun getConsentstring(context: Context): String

    /**
     * Export consent string
     *
     * @return saved Cmp String
     */
    fun exportCmpString(): String


    fun importCmpString(cmpString: String, importCallback: CmpImportCallback)


    /**
     * Get called last
     *
     * @param context Application Context
     * @return last called Cmp Layer Date
     */// Deprecated
    fun getCalledLast(context: Context): Date?


    /**
     * Called this day
     *
     * @param context Application Context
     * @return if layer was called today
     */
    fun calledThisDay(context: Context): Boolean

    /**
     * Needs acceptance
     *
     * @param context Application Context
     * @return if user needs Acceptance
     */
    fun needsAcceptance(context: Context): Boolean

    /**
     * opens the consent layer
     *
     * @param context Application Context
     * @param gdprButton action Button
     * @param callback onClose Callback
     */
    @Deprecated(message = "This function will be removed in the future")
    fun setOpenCmpConsentToolViewListener(
        context: Context,
        gdprButton: Button,
        callback: OnCloseCallback?,
    )


    /**
     * Set open place holder view listener
     *
     * @param context Application Context
     * @param actionButton
     * @param vendor
     */
    @Deprecated(message = "This function will be removed in the future")
    fun setOpenPlaceHolderViewListener(context: Context, actionButton: Button, vendor: String)

}
package net.consentmanager.consentmanager

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import net.consentmanager.sdk.CMPConsentTool.Companion.createInstance
import net.consentmanager.sdk.common.callbacks.OnCMPNotOpenedCallback
import net.consentmanager.sdk.common.callbacks.OnCloseCallback
import net.consentmanager.sdk.common.callbacks.OnOpenCallback
import net.consentmanager.sdk.consentlayer.model.CMPConfig

class NewLanguageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_language)

        findViewById<Button>(R.id.btn_de).setOnClickListener {
            openCmp("DE")
        }

        findViewById<Button>(R.id.btn_en).setOnClickListener {
            openCmp("EN")
        }
    }

    private fun openCmp(language: String) {
        val consentTool = createInstance(
            context = applicationContext,
            codeId = "201",
            serverDomain = "delivery.consentmanager.net",
            appName = "Test App",
            lang = language,
            timeout = 7000,
            openListener = object : OnOpenCallback {
                override fun onWebViewOpened() {
                    Log.d("+++", "+++ onWebViewOpened")
                }
            },
            closeListener = object : OnCloseCallback {
                override fun onWebViewClosed() {
                    Log.d("+++", "+++ onWebViewClosed")
                    AlertDialog.Builder(this@NewLanguageActivity).setMessage("OK!").show()
                }
            },
            cmpNotOpenedCallback = object : OnCMPNotOpenedCallback {
                override fun onCMPNotOpened() {
                    Log.d("+++", "onCMPNotOpened")
                }

            }
        )
        CMPConfig.deactivateCustomLayer()
        consentTool.openCmpConsentToolView(applicationContext)
    }
}
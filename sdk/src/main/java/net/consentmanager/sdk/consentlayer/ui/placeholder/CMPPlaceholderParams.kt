package net.consentmanager.sdk.consentlayer.ui.placeholder

import net.consentmanager.sdk.common.callbacks.CmpPlaceholderEventListener

@Deprecated(message = "This Class will be removed in the future")
class CMPPlaceholderParams private constructor(val vendorId: String) {
    private var text: String? = null
    private var headline: String? = null
    private var buttonText: String? = null
    private var checkboxText: String? = null
    private var imageUrl: String? = null
    private val cmpPlaceholderEventListener: CmpPlaceholderEventListener? = null
    fun setCustomPlaceholder(
        headline: String?,
        mainText: String?,
        checkboxText: String?,
        buttonText: String?
    ): CMPPlaceholderParams {
        this.buttonText = buttonText
        this.headline = headline
        text = mainText
        this.checkboxText = checkboxText
        return this
    }

    fun addOptionalImageUrl(imageUrl: String?): CMPPlaceholderParams {
        this.imageUrl = imageUrl
        return this
    }

    val customPlaceholderToGetParam: String
        get() {
            val buttonTextParam = "&btn=%s"
            val checkboxParam = "&check=%s"
            val textParam = "&txt=%s"
            val headlineParam = "&hl=%s"
            val url = StringBuilder()
            if (buttonText != null) {
                url.append(String.format(buttonTextParam, buttonText))
            }
            if (checkboxText != null) {
                url.append(String.format(checkboxParam, checkboxText))
            }
            if (text != null) {
                url.append(String.format(textParam, text))
            }
            if (headline != null) {
                url.append(String.format(headlineParam, headline))
            }
            return url.toString()
        }
    val imageUrlToGetParam: String
        get() = if (imageUrl != null) {
            String.format("&img=%s", imageUrl)
        } else ""

    override fun toString(): String {
        return "CMPPlaceholderParams{" +
                "vendorId='" + vendorId + '\'' +
                ", text='" + text + '\'' +
                ", headline='" + headline + '\'' +
                ", buttonText='" + buttonText + '\'' +
                ", checkboxText='" + checkboxText + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", cmpPlaceholderEventListener=" + cmpPlaceholderEventListener +
                '}'
    }

    companion object {
        fun ofVendor(id: String): CMPPlaceholderParams {
            return CMPPlaceholderParams(id)
        }
    }
}
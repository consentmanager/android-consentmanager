package net.consentmanager.sdk.consentlayer.model

import androidx.annotation.IdRes
import androidx.annotation.Keep

internal const val DEFAULT_TIMEOUT = 7500
internal const val DEFAULT_CONTAINER_VIEW_ID = -1
internal const val DEFAULT_CUSTOM_LAYER_COLOR = "#FFFFFF"

@Keep
object CMPConfig : CmpConfigInterface {
    var isCustomLayer = false
    var id: String = ""
    var idfa: String? = null
    var serverDomain: String? = null
    var appName: String? = null
    var language: String? = null
    var timeout: Int = DEFAULT_TIMEOUT
    var skipToCustomizePage = false
    var containerViewId = DEFAULT_CONTAINER_VIEW_ID
    var customLayerColor = DEFAULT_CUSTOM_LAYER_COLOR
    var designId: Int? = null
    var debug: Boolean? = false

    override fun isValid(): Boolean {
        return id != "" &&
                !serverDomain.isNullOrEmpty() &&
                !appName.isNullOrEmpty()
    }

    override fun activateCustomLayer(@IdRes containerViewId: Int): CMPConfig {
        this.containerViewId = containerViewId
        isCustomLayer = true
        return this
    }

    override fun deactivateCustomLayer() {
        isCustomLayer = false
    }

    override fun toString(): String {
        return "CMPConfig{id=$id, idfa='$idfa', serverDomain='$serverDomain', appName='$appName', language='$language', timeout=$timeout}"
    }

    override fun setCustomViewContainerId(containerViewId: Int): CMPConfig {
        isCustomLayer = true
        this.containerViewId = containerViewId
        return this
    }

    override fun enableSettingsPage() {
        skipToCustomizePage = true
    }

    override fun removeCustomViewContainerId() {
        isCustomLayer = false
        containerViewId = -1
    }

    override fun reset() {
        isCustomLayer = false
        id = ""
        idfa = null
        serverDomain = null
        appName = null
        language = null
        timeout = DEFAULT_TIMEOUT
        skipToCustomizePage = false
        containerViewId = DEFAULT_CONTAINER_VIEW_ID
        customLayerColor = DEFAULT_CUSTOM_LAYER_COLOR
        debug = false
    }

}
package net.consentmanager.sdk.consentlayer.model

import net.consentmanager.sdk.consentlayer.model.valueObjects.Purpose
import net.consentmanager.sdk.consentlayer.model.valueObjects.Vendor
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

internal class CmpConsentTest {

    private lateinit var cmpConsent: CmpConsent

    private val sampleJson = """{
        "cmpString": "123",
        "addtlConsent": "foo",
        "consentstring": "bar",
        "gdprApplies": true,
        "googleVendorConsents": {"foo": true, "bar": false},
        "hasGlobalScope": true,
        "publisherCC": "baz",
        "regulation": 1,
        "regulationKey": "key",
        "tcfcompliant": true,
        "tcfversion": 2,
        "uspstring": "baz",
        "vendorsList": [{"id": "foo", "name": "Foo", "iabid": "1", "systemid": "1", "googleid": "1", "policyUrl": "http://foo.com/privacy"}, {"id": "bar", "name": "Bar", "iabid": "1", "systemid": "1", "googleid": "1", "policyUrl": "http://bar.com/privacy"}],
        "purposeLI": {"foo": true, "bar": false},
        "vendorLI": { "foo": true, "bar" : false},
        "purposesList": [{"id": "foo", "name": "Foo", "description": "This is Foo"}, {"id": "bar", "name": "Bar", "description": "This is Bar"}],
        "vendorConsents": {"foo": true, "bar": false},
        "purposeConsents": {"foo": true, "bar": false},
        "metadata": [{"key": "foo", "value": "bar"}],
        "userChoiceExists": true,
        "tcfcaversion": "",
        "gppversions": [],
        "purModeActive": false,
        "purModeLoggedIn": false,
        "purModeLogic": "",
        "consentExists": false
    }"""

    @Test
    fun testFromJson() {
        val consent = CmpConsent.fromJson(sampleJson)
        assertEquals("123", consent.getCmpStringBase64Encoded())
        assertTrue(consent.gdprApplies)
        assertEquals("bar", consent.consentString)
        assertEquals("foo", consent.googleAdditionalConsent)
        assertEquals(mapOf("foo" to true, "bar" to false), consent.googleVendorList)
        assertTrue(consent.hasGlobalScope)
        assertEquals("baz", consent.publisherCC)
        assertEquals(1, consent.regulation)
        assertEquals("key", consent.regulationKey)
        assertTrue(consent.tcfCompliant)
        assertEquals(2, consent.tcfVersion)
        assertEquals("baz", consent.uspString)
        assertEquals(2, consent.getAllVendor().size)
        assertTrue(consent.hasPurpose("foo"))
        // even if set to false, only the keys are interesting for the result
        assertTrue(consent.hasPurpose("bar"))
        assertTrue(consent.hasVendor("foo"))
        // even if set to false, only the keys are interesting for the result
        assertTrue(consent.hasVendor("bar"))
        assertEquals(1, consent.getMetadata().size)
    }

    @Test
    fun testEmptyConsent() {
        val consent = CmpConsent.emptyConsent()
        assertTrue(consent.getAllVendor().isEmpty())
        assertTrue(consent.getAllPurposes().isEmpty())
        assertTrue(consent.getDisabledPurposes().isEmpty())
        assertTrue(consent.getDisabledVendors().isEmpty())
        assertTrue(consent.getEnabledPurposes().isEmpty())
        assertTrue(consent.getEnabledVendors().isEmpty())
        assertTrue(consent.isEmpty())
        assertFalse(consent.hasConsent())
    }

    @Before
    fun setUp() {
        cmpConsent = CmpConsent.emptyConsent()
    }


    @Test
    fun `hasPurpose should return true if the given purpose id is in the purposeMap`() {
        cmpConsent = cmpConsent.copy(enabledPurposes = mapOf("1" to true, "2" to false))
        assertTrue(cmpConsent.hasPurpose("1"))
    }

    @Test
    fun `hasPurpose should return false if the given purpose id is not in the purposeMap`() {
        cmpConsent = cmpConsent.copy(enabledPurposes = mapOf("1" to true, "2" to false))
        assertFalse(cmpConsent.hasPurpose("3"))
    }

    @Test
    fun `hasVendor should return true if the given vendor id is in the agreedVendors map`() {
        cmpConsent = cmpConsent.copy(enabledVendors = mapOf("1" to true, "2" to false))
        assertTrue(cmpConsent.hasVendor("1"))
    }

    @Test
    fun `hasVendor should return false if the given vendor id is not in the agreedVendors map`() {
        cmpConsent = cmpConsent.copy(enabledVendors = mapOf("1" to true, "2" to false))
        assertFalse(cmpConsent.hasVendor("3"))
    }

    @Test
    fun `getCmpId should return an empty string if cmpId is null`() {
        cmpConsent = cmpConsent.copy(cmpStringBase64Encoded = null)
        assertEquals("", cmpConsent.getCmpStringBase64Encoded())
    }

    @Test
    fun `getCmpId should return cmpId if it is not null`() {
        cmpConsent = cmpConsent.copy(cmpStringBase64Encoded = "1")
        assertEquals("1", cmpConsent.getCmpStringBase64Encoded())
    }

    @Test
    fun `isValid should return true if the consentString is not empty`() {
        cmpConsent = cmpConsent.copy(consentString = "consent")
        assertTrue(cmpConsent.isValid())
    }

    @Test
    fun `isValid should return false if the consentString is empty`() {
        assertFalse(cmpConsent.isValid())
    }

    @Test
    fun `getAllPurposes should return a list of all purpose ids`() {
        cmpConsent = cmpConsent.copy(allPurposesList = listOf(Purpose("1"), Purpose("2")))
        assertEquals(listOf("1", "2"), cmpConsent.getAllPurposes())
    }

    @Test
    fun `getAgreedPurposes should return a list of all agreed purpose ids`() {
        cmpConsent = cmpConsent.copy(enabledPurposes = mapOf("1" to true, "2" to false))
        assertEquals(listOf("1", "2"), cmpConsent.getEnabledPurposes())
    }

    @Test
    fun `getDisabledPurposes should return a list of all disabled purpose ids`() {
        cmpConsent = cmpConsent.copy(
            allPurposesList = listOf(Purpose("1"), Purpose("2")),
            enabledPurposes = mapOf("1" to true)
        )
        assertEquals(listOf("2"), cmpConsent.getDisabledPurposes())
    }

    @Test
    fun `getDisabledVendors should return a list of all disabled vendor ids`() {
        cmpConsent =
            cmpConsent.copy(
                allVendorsList = listOf(Vendor("1", "1", "1", "1", name = "test")),
                enabledVendors = mapOf("1" to true)
            )
        assertTrue(cmpConsent.getDisabledVendors().isEmpty())
    }
}
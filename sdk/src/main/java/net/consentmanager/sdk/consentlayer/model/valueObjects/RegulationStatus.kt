package net.consentmanager.sdk.consentlayer.model.valueObjects

/**
 * Regulation status
 *
 * @property value
 * @constructor Create empty Regulation status
 */
enum class RegulationStatus(val value: Int) {
    /**
     * Ccpa Applies
     *
     * @constructor Create empty Ccpa Applies
     */
    CCPA_APPLIES(2),

    /**
     * Gdpr Applies
     *
     * @constructor Create empty Gdpr Applies
     */
    GDPR_APPLIES(1),

    /**
     * Unknown
     *
     * @constructor Create empty Unknown
     */
    UNKNOWN(0);

    companion object {
        private val map = values().associateBy(RegulationStatus::value)

        @JvmStatic
        fun fromInt(type: Int) = map[type]
    }
}
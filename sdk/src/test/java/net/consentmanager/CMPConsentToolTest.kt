package net.consentmanager

import android.content.Context
import net.consentmanager.sdk.CMPConsentTool
import net.consentmanager.sdk.CMPConsentTool.Companion.createInstance
import net.consentmanager.sdk.consentlayer.model.CMPConfig
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class CMPConsentToolTest {

    @Mock
    private lateinit var mockContext: Context

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        //clean CMPConfig
        CMPConfig.reset()
        CMPConsentTool.setInstance(null)
    }


    @Test(expected = IllegalStateException::class)
    fun `getInstance returns Error when no instance exists`() {
        CMPConsentTool.getInstance(mockContext)
    }

    @Test
    fun testCreateInstanceWithValidConfig() {
        val config = CMPConfig.apply {
            id = "1"
            serverDomain = "example.com"
            appName = "Example App"
            language = "en"
            idfa = "idfa"
            timeout = 5000
        }
        val consentTool = createInstance(mockContext, config = config)

        assertNotNull(consentTool)
        assertEquals(config, consentTool.config)
    }

    @Test
    fun testCreateInstanceWithInvalidConfig() {
        val config = CMPConfig.apply {
            id = "1"
            serverDomain = null // Set serverDomain to null, which is an invalid value
            appName = "Example App"
            language = "en"
            idfa = "idfa"
            timeout = 5000
        }

        val exception = assertThrows(IllegalStateException::class.java) {
            createInstance(mockContext, config = config)
        }

        assertEquals("CMPConfig values are not valid", exception.message)
    }

    @Test
    fun `createInstance with valid configuration`() {
        val consentTool = CMPConsentTool.Companion.createInstance(
            context = mockContext,
            codeId = "123",
            serverDomain = "http://example.com",
            appName = "My App",
            lang = "en-US",
        )

        assertNotNull(consentTool)
        assertEquals("123", CMPConfig.id)
        assertEquals("http://example.com", CMPConfig.serverDomain)
        assertEquals("My App", CMPConfig.appName)
        assertEquals("en-US", CMPConfig.language)
    }

    @Test(expected = IllegalStateException::class)
    fun `createInstance with invalid configuration`() {
        createInstance(
            context = mockContext,
            codeId = "0",
            serverDomain = null,
            appName = null,
            lang = null
        )
    }

    @Test
    fun `getInstance returns existing instance`() {
        // Create instance
        createInstance(
            context = mockContext,
            codeId = "123",
            serverDomain = "http://example.com",
            appName = "My App",
            lang = "en-US"
        )

        // Get instance
        val consentTool = CMPConsentTool.getInstance(mockContext)

        assertNotNull(consentTool)
        assertEquals("123", CMPConfig.id)
        assertEquals("http://example.com", CMPConfig.serverDomain)
        assertEquals("My App", CMPConfig.appName)
        assertEquals("en-US", CMPConfig.language)
    }

    @Test
    fun `getInstance with config parameter returns existing instance`() {
        // Create instance
        createInstance(
            context = mockContext,
            codeId = "123",
            serverDomain = "http://example.com",
            appName = "My App",
            lang = "en-US"
        )

        // Get instance with config parameter
        val config = CMPConfig
        config.id = "456"
        val consentTool = CMPConsentTool.getInstance(mockContext, config)

        assertNotNull(consentTool)
        assertEquals("456", CMPConfig.id)
        assertEquals("http://example.com", CMPConfig.serverDomain)
        assertEquals("My App", CMPConfig.appName)
        assertEquals("en-US", CMPConfig.language)
    }

    @Test
    fun `getInstance returns new instance when no instance exists but CMPConfig is set`() {
        CMPConfig.apply {
            id = "123"
            serverDomain = "http://example.com"
            appName = "My App"
            language = "en-US"
        }
        val consentTool = CMPConsentTool.getInstance(mockContext)

        assertNotNull(consentTool)
        assertTrue(CMPConfig.isValid())
    }

    @Test
    fun `getInstance with config parameter returns new instance when no instance exists`() {
        val config = CMPConfig
        config.id = "123"
        config.serverDomain = "http://example.com"
        config.appName = "My App"
        config.language = "en-US"

        val consentTool = CMPConsentTool.getInstance(mockContext, config)

        assertNotNull(consentTool)
        assertEquals("123", CMPConfig.id)
        assertEquals("http://example.com", CMPConfig.serverDomain)
        assertEquals("My App", CMPConfig.appName)
        assertEquals("en-US", CMPConfig.language)
    }

}
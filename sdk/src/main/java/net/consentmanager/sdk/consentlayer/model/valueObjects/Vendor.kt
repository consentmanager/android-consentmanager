package net.consentmanager.sdk.consentlayer.model.valueObjects

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Vendor
 *
 * @property id
 * @property iabId
 * @property systemId
 * @property googleId
 * @property name
 * @constructor Create empty Vendor
 */
@Keep
@Serializable
class Vendor(
    @SerialName("id")
    val id: String,
    @SerialName("iabid")
    val iabId: String,
    @SerialName("systemid")
    val systemId: String,
    @SerialName("googleid")
    val googleId: String,
    @SerialName("name")
    val name: String
) {
    override fun toString(): String {
        return "Vendor(id='$id', iabId='$iabId', systemId='$systemId', googleId='$googleId', name='$name')"
    }
}
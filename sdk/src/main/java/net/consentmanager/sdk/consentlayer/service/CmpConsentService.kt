package net.consentmanager.sdk.consentlayer.service

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import kotlinx.serialization.json.Json
import net.consentmanager.sdk.common.CmpError
import net.consentmanager.sdk.common.callbacks.CmpCallbackWrapper
import net.consentmanager.sdk.consentlayer.CmpState
import net.consentmanager.sdk.consentlayer.model.CmpConsent
import net.consentmanager.sdk.consentlayer.model.valueObjects.CmpMetadata
import net.consentmanager.sdk.consentlayer.model.valueObjects.RegulationStatus
import net.consentmanager.sdk.consentlayer.model.valueObjects.mapButtonEvent
import net.consentmanager.sdk.consentlayer.repository.CmpRepository
import java.util.Date

private const val TAG = "CMP:ConsentService"
private const val CMP_CONSENT_ID = "CMP_CONSENT_ID"
private const val CMP_METALIST = "CMP_METALIST"
private const val SEPARATOR = ","

/**
 * Cmp consent service
 *
 * @property mContext Application Context
 * @constructor Create empty Cmp consent service
 */
internal class CmpConsentService(
    private val mContext: Context,
) : CmpServiceInterface {
    private val cmpPreferences: CmpPreferences = CmpPreferences(mContext)

    init {
        callbackWrapper = CmpCallbackWrapper
    }

    /**
     * Set callbacks
     *
     * @param callbackWrapper
     * @return
     */
    fun setCallbacks(callbackWrapper: CmpCallbackWrapper): CmpConsentService {
        CmpConsentService.callbackWrapper = callbackWrapper
        return this
    }

    /**
     * Saving a Consent
     * ! The json can be only provide by the Consentmanager API from
     *
     * @param cmpConsentJson the internal cmp Consent Object as Json
     */
    override fun saveConsentJson(cmpConsentJson: String): Boolean {
        return try {
            val cmpConsent: CmpConsent = CmpConsent.fromJson(cmpConsentJson)
            Log.d(TAG, "saving Consent: $cmpConsentJson")
            saveConsent(cmpConsent)
            true
        } catch (e: IllegalArgumentException) {
            onErrorOccurred(CmpError.ConsentReadWriteError, e.message ?: "Error while parsing JSON")
            resetAll(mContext)
            false
        }

    }

    /**
     * Save consent
     *
     * @param cmpConsent
     */
    override fun saveConsent(cmpConsent: CmpConsent) {
        saveMetadata(mContext, cmpConsent.getMetadata())
        persistConsent(cmpConsent)
        CmpRepository.setLastRequested(mContext, Date())
        // check API after saving consent need to return false
        CmpRepository.setCheckApiResponse(mContext, false)
        handleWebViewState(cmpConsent)
    }

    private fun persistConsent(cmpConsent: CmpConsent) {
        try {
            val json = cmpConsent.toJson()
            cmpPreferences.setString(CMP_CONSENT_ID, json)
            CmpState.actionConsentReceived()
        } catch (e: IllegalArgumentException) {
            onErrorOccurred(
                CmpError.ConsentReadWriteError,
                "Error while persisting Consent. Clear all values..."
            )
            resetAll(mContext)
        }
    }

    private fun handleWebViewState(cmpConsent: CmpConsent) {
        val buttonEvent = mapButtonEvent(cmpConsent)
        if (CmpState.isOpen()) {
            postMainThreadCallback(callbackWrapper.onCloseCallback::onWebViewClosed)
            Handler(Looper.getMainLooper()).post {
                callbackWrapper.onCmpButtonClickedCallback.onButtonClicked(buttonEvent)
            }
            CmpState.actionLayerClosed()
        } else {
            postMainThreadCallback(callbackWrapper.onNotOpenActionCallback::onCMPNotOpened)
            Handler(Looper.getMainLooper()).post {
                callbackWrapper.onCmpButtonClickedCallback.onButtonClicked(buttonEvent)
            }
        }
    }

    private fun postMainThreadCallback(callback: () -> Unit) {
        Handler(Looper.getMainLooper()).post {
            callback()
        }
    }

    private fun saveMetadata(context: Context, meta: List<CmpMetadata>) {
        val cmpPreferences =
            CmpPreferences(context)
        //saving list of meta keys
        cmpPreferences.setString(
            CMP_METALIST,
            meta.map { x -> x.key }.joinToString(separator = SEPARATOR)
        )
        //saving meta key - value pairs
        for (metadata in meta) {
            when (metadata.type) {
                "int" -> {
                    val intValue =
                        runCatching { Integer.parseInt(metadata.value!!) }.getOrElse { -1 }
                    cmpPreferences.setInt(metadata.key!!, intValue)
                }

                "string" -> {
                    cmpPreferences.setString(metadata.key!!, metadata.value!!)
                }

                "bool" -> {
                    cmpPreferences.setBoolean(metadata.key!!, metadata.value!!.toBoolean())
                }

                else -> {
                    cmpPreferences.setString(metadata.key!!, metadata.value!!)
                }
            }
        }
    }

    companion object {
        lateinit var callbackWrapper: CmpCallbackWrapper

        private fun removeCmpConsentDto(context: Context) {
            (CmpPreferences(context)).removeKey(CMP_CONSENT_ID)
        }


        fun getCmpConsent(context: Context): CmpConsent {
            val sharedPreferences = CmpPreferences(context)
            return try {
                val cmpJson: String = sharedPreferences.getString(CMP_CONSENT_ID, "")
                if (cmpJson != "") {
                    val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
                    jsonConfig.decodeFromString(CmpConsent.serializer(), cmpJson)
                } else {
                    CmpConsent.emptyConsent()
                }
            } catch (e: IllegalArgumentException) {
                CmpCallbackWrapper.onErrorCallback.errorOccurred(
                    CmpError.ConsentReadWriteError,
                    "Error while parsing Consent. Consent will be reset"
                )
                resetAll(context)
                CmpConsent.emptyConsent()
            }
        }

        fun resetAll(context: Context) {
            removeCmpConsentDto(context)
            removeMetaData(context)
            CmpRepository.reset(context)
        }

        private fun removeMetaData(context: Context) {
            val sharedPreferences = CmpPreferences(context)
            val cmpMetaList: List<String> =
                sharedPreferences.getString(CMP_METALIST, "").split(SEPARATOR)

            cmpMetaList.forEach { metadata ->
                sharedPreferences.removeKey(metadata)
            }
        }
    }

    /**
     * Get cmp consent
     *
     * @return
     */
    fun getCmpConsent(): CmpConsent {
        return try {
            val cmpJson: String = cmpPreferences.getString(CMP_CONSENT_ID, "")
            if (cmpJson != "") {
                val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
                jsonConfig.decodeFromString(CmpConsent.serializer(), cmpJson)
            } else {
                CmpConsent.emptyConsent()
            }
        } catch (e: IllegalArgumentException) {
            CmpCallbackWrapper.onErrorCallback.errorOccurred(
                CmpError.ConsentReadWriteError,
                "Error while parsing Consent. Consent will be reset"
            )
            resetAll(mContext)
            CmpConsent.emptyConsent()
        }
    }

    /**
     * Get consent string
     *
     * @return
     */
    fun getCmpString(): String {
        return getCmpConsent().getCmpStringBase64Encoded()
    }

    /**
     * On open action
     *
     */
    override fun onOpenAction() {
        Log.v(TAG, "Consent layer is opening")
        CmpState.actionLayerOpened()
        Handler(Looper.getMainLooper()).post {
            callbackWrapper.onOpenCallback.onWebViewOpened()
        }
    }

    /**
     * On close action
     *
     */
    override fun onCloseAction() {
        Log.v(TAG, "Consent layer is closing")
        CmpState.actionLayerClosed()
        Handler(Looper.getMainLooper()).post {
            callbackWrapper.onCloseCallback.onWebViewClosed()
        }
    }

    /**
     * On error occurred
     *
     * @param message
     */
    fun onErrorOccurred(type: CmpError, message: String) {
        Log.v(TAG, "Consent layer has an error: $type")
        Handler(Looper.getMainLooper()).post {
            callbackWrapper.onErrorCallback.errorOccurred(type, message)
        }
    }

    /**
     * On load cmp layer
     *
     */
    fun onLoadCmpLayer() {
        Log.v(TAG, "Consentlayer is loading")
        CmpState.actionLayerLoading()
    }

    /**
     * Save check api need open layer
     *
     * @param response
     */
    fun saveCheckApiNeedOpenLayer(response: Boolean) {
        CmpRepository.setCheckApiResponse(mContext, response)
        CmpRepository.setCheckApiLastUpdate(mContext)
    }

    /**
     * Last check api update
     *
     * @return
     */
    fun lastCheckApiUpdate(): Date? {
        return CmpRepository.getLastCheckApiUpdate(mContext)
    }

    /**
     * Get check api response
     *
     * @return
     */
    fun getCheckApiResponse(): Boolean {
        return CmpRepository.getCheckApiResponse(mContext)
    }

    /**
     * Get regulation status
     *
     * @return
     */
    fun getRegulationStatus(): RegulationStatus {
        return RegulationStatus.fromInt(getCmpConsent().regulation)!!
    }

    /**
     * Handle called today
     *
     * @param context
     */
    fun handleCalledToday(context: Context) {
        postMainThreadCallback(callbackWrapper.onNotOpenActionCallback::onCMPNotOpened)
        Handler(Looper.getMainLooper()).post {
            callbackWrapper.onCmpButtonClickedCallback.onButtonClicked(
                mapButtonEvent(
                    getCmpConsent(context)
                )
            )
        }
    }
}
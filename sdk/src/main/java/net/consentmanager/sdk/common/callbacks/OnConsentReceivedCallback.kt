package net.consentmanager.sdk.common.callbacks

/**
 * On consent received callback
 *
 */
interface OnConsentReceivedCallback {

    /**
     * On finish
     *
     */
    fun onFinish()
}
package net.consentmanager.sdk.consentlayer.model

import androidx.annotation.IdRes

/**
 * Cmp config interface
 *
 * @constructor Create empty Cmp config interface
 */
interface CmpConfigInterface {

    /**
     * Is valid
     *
     * @return if config is valid
     */
    fun isValid(): Boolean

    /**
     * Activate custom layer
     *
     * @param containerViewId
     * @return CmpConfigInterface
     */
    fun activateCustomLayer(@IdRes containerViewId: Int): CmpConfigInterface

    /**
     * Deactivate custom layer
     *
     */
    fun deactivateCustomLayer()


    /**
     * Set custom view container id
     *
     * @param containerViewId
     * @return CmpConfigInterface
     */
    fun setCustomViewContainerId(containerViewId: Int): CmpConfigInterface

    fun enableSettingsPage()

    /**
     * Remove custom view container id
     *
     */
    fun removeCustomViewContainerId()

    /**
     * Reset
     *
     */
    fun reset()
}